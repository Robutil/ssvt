
module Lab1 where
import Data.List
import Test.QuickCheck

prime :: Integer -> Bool
prime n = n > 1 && all (\ x -> rem n x /= 0) xs
  where xs = takeWhile (\ y -> y^2 <= n) primes

primes :: [Integer]
primes = 2 : filter prime [3..]

infix 1 -->

(-->) :: Bool -> Bool -> Bool
p --> q = (not p) || q

forall :: [a] -> (a -> Bool) -> Bool
forall = flip all

reversal :: Integer -> Integer
reversal = read . reverse . show

-- ex 1 (20 minutes, but few hours to get env fixed)
--part 1
sumSquares :: Integer -> Integer
sumSquares n = sum [ k^2 | k <- [1..n] ]

sumSquares' :: Integer -> Integer
sumSquares' n = (n*(n+1)*(2*n+1)) `div` 6

testSumSquares :: Integer -> Bool
testSumSquares n = n>0 --> sumSquares n == sumSquares' n

--part 2
sumKubes :: Integer -> Integer
sumKubes n = sum [ k^3 | k <- [1..n] ]

sumKubes' :: Integer -> Integer
sumKubes' n = (n*(n+1) `div` 2)^2

testSumKubes :: Integer -> Bool
testSumKubes n =n>0 --> sumKubes n == sumKubes' n

-- ex 2 30 mins
sumSubSeq :: Integer -> Int
sumSubSeq n = 2^n

sumSubSeq' :: Integer -> Int
sumSubSeq' n = length(subsequences[1..n])

testSumSubSeq :: Integer -> Bool
testSumSubSeq n = n>0&&n<20 --> sumSubSeq n == sumSubSeq' n

-- The property is hard to test because the test got stuck sometimes, so i added a check that its <100 as well.
--2 ^ 2000 propbably causes some kind of integer overflow

-- I believe we are testing the mathematical fact of the formula, we use subsequences
-- but assume it works correct and check in doing so as well

-- ex 3 50 minutes
sumPerms :: Integer -> Integer
sumPerms n = product [1..n]

sumPerms' :: Integer -> Integer
sumPerms' n = fromIntegral(length(permutations[1..n]))

testSumPerms :: Integer -> Bool
testSumPerms n = n>=0&&n<10 --> sumPerms n == sumPerms' n
-- The same applies as the previous exercise, the operation is so expensive it only works on small numbers

-- Same as exercise 2.

--Exercise 4 40 minutes
getReversablePrimes :: [Integer]
getReversablePrimes = [x | x <- [1..10000], prime x && prime(reversal x)]

-- I think I would test this function by manually checking the numbers since for all <10.000 this is still doable
-- For more numbers, you'd want an automated test but basicly it would do that same as the conditional in the mapping function

--One thing that is not clearly specified is how reversal should work for 1 digit numbers, since they are the same when reversed

--Exercise 5 50 minutes
-- Get the input nr + next hundred primes
getNextHundredPrimes :: Integer -> [Integer]
getNextHundredPrimes n  = take 101 [x | x<-[n..], prime x]

getSummedPrime :: Integer
getSummedPrime = head (take 1 [x | x<-[0..], prime x && prime(sum(getNextHundredPrimes x))])

checkSummedPrimePrimes :: [Integer]
checkSummedPrimePrimes = getNextHundredPrimes getSummedPrime

checkSumSummedPrimePrimes :: Integer
checkSumSummedPrimePrimes = sum checkSummedPrimePrimes

-- The most straightforward way to test this is to check that all of the numbers, and the resulting number are primes
-- Which I took a quick look over and the result 37447 is a prime at least.
-- To check that this is indeed the lowest is a lot more difficult


--Exercise 6 30 minutes
getNextNrPrimes :: Int -> [Integer]
getNextNrPrimes n  = take n [x | x<-[2..], prime x]

getProductOfPrimes :: [Integer] -> Integer
getProductOfPrimes xs = foldr (*) 1 xs

getRefutingExample :: Int
getRefutingExample = head (take 1 [x | x<-[1..], not (prime(getProductOfPrimes(getNextNrPrimes x) +1) ) ] )
-- The lowest number found is 6. This is the  list [2,3,5,7,11,13],
-- the product of this list is 30030, so 30031 which is divisable by 59 and 509 so this refutes the statement

-- I think the best way to test this is to mainly test the individual
-- methods used and to also let it validate the results generated

--Exercise 7 60 minutes
splitIntoDigs :: Integer -> [Integer]
splitIntoDigs 0 = []
splitIntoDigs n = splitIntoDigs (n `div` 10) ++ [n `mod` 10]

luhnDouble :: Integer -> Integer
luhnDouble n = if n*2 > 9
  then (2*n)-9
    else 2 * n

luhnDoubleAlternating :: Integer -> [Integer] -> [Integer]
luhnDoubleAlternating n [] = []
luhnDoubleAlternating n (x:xs) = if n == 0
  then x : luhnDoubleAlternating 1 xs
else luhnDouble x : luhnDoubleAlternating 0 xs

getCheckSumDigit :: Integer -> Integer
getCheckSumDigit n = head (splitIntoDigs (reversal n))

getLuhnDigits :: Integer -> [Integer]
getLuhnDigits n = luhnDoubleAlternating 1 (tail (splitIntoDigs (reversal n)))

luhn :: Integer -> Bool
luhn n =  getCheckSumDigit n == sum(getLuhnDigits n) * 9 `mod` 10
--Source: https://stackoverflow.com/a/1918522
joiner :: [Integer] -> Integer
joiner = read . concatMap show

isAmericanExpress :: Integer -> Bool
isAmericanExpress n = length(splitIntoDigs n)== 15 && luhn n

isMaster :: Integer -> Bool
isMaster n = length(splitIntoDigs n)== 16 && luhn n && ((joiner (take 2 (splitIntoDigs n)) >= 51 &&  joiner(take 2 (splitIntoDigs n)) <= 55) || (joiner(take 4 (splitIntoDigs n)) >= 2221 &&  joiner(take 4 (splitIntoDigs n)) <= 2720))

isVisa  :: Integer -> Bool
isVisa  n = length(splitIntoDigs n)== 16 && luhn n && joiner (take 1 (splitIntoDigs n)) == 4

-- Tested with the wikis example 7992739871, without its check digit 3 it is false, with it we return true
-- For every other check digit we return false
-- I think a good way to test these functions would be to get a couple dozen of numbers which are correct
-- Then randomly substitute the CheckSumDigits and verify that this returns false
-- For visa/master/AE :
-- Some examples can be found here https://www.freeformatter.com/credit-card-number-generator-validator.html

--Exercise 8

data Boy = Matthew | Peter | Jack | Arnold | Carl
           deriving (Eq,Show)

boys = [Matthew, Peter, Jack, Arnold, Carl]

accuses :: Boy -> Boy -> Bool
accuses Matthew Carl = False
accuses Matthew Matthew = False
accuses Matthew otherBoy = True --Guess he defaults to true for the others?
accuses Peter Matthew = True
accuses Peter Jack = True
accuses Jack otherBoy = not (accuses Matthew otherBoy) && not (accuses Peter otherBoy)
accuses Arnold otherBoy = accuses Matthew otherBoy /= accuses Peter otherBoy
accuses Carl otherBoy = not (accuses Arnold otherBoy)
accuses _ _ = False

accusers :: Boy -> [Boy]
accusers boy = [  x | x<- boys, x `accuses` boy]

-- Attempt to find situation where 3 boys are speaking the truth about him being guilty
honestBoy :: Boy -> [Boy]
honestBoy boy = [x | x <- boys, x `accuses` boy]

guilty :: [Boy]
guilty = [x | x <- boys, length(honestBoy x) >= 3]

honest  :: [Boy]
honest = [x | x <- boys, x `accuses` head guilty]

main = do
  quickCheck testSumSquares
  quickCheck testSumKubes
  quickCheck testSumPerms
  print getReversablePrimes
