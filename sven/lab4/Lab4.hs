
module Lab4 where

import Data.List
import System.Random
import Test.QuickCheck
import SetOrd
import Debug.Trace

--Q1 2-3 hours
genZeroToTen :: IO Int
genZeroToTen =
  getStdRandom (randomR(0,10))

genList :: Int -> [IO Int]
genList n = if n == 0
  then []
  else genZeroToTen : genList(n-1)

{-- Work in progress
convertList :: IO [Int] -> [Int]
convertList xs = do
  ys <- xs
  return ys

generateSetCustom :: [Int] -> Set Int
generateSetCustom xs = list2set xs
--}

--To use quickcheck to gen the lists for us, we simply ask for [Int], then list2set!
generateSetQuickCheck :: [Int] -> Set Int
generateSetQuickCheck = list2set

--Test if the set is ordered
prop_set_ordered :: Set Int -> Bool
prop_set_ordered (Set []) = True
prop_set_ordered (Set [x]) = True
prop_set_ordered (Set (x:y:xs)) = {--trace (show x)--} (x <= y) && prop_set_ordered (Set(y:xs))

--Test if the set has no duplicates. We assume that it is ordered, then we can bassicly check if the same value does not repeat
prop_set_no_duplicates :: Set Int -> Bool
prop_set_no_duplicates (Set []) = True
prop_set_no_duplicates (Set [x]) = True
prop_set_no_duplicates (Set (x:y:xs)) = (x /= y) && prop_set_ordered (Set(y:xs))

--Excercise 2 Time spend: 2-3 hours. I did end up using a tuple of sets as input since I had some issues generating
--2 seperate sets with quickcheck
setUnion :: (Set Int, Set Int) -> Set Int
setUnion (a,b)= unionSet a b

intersectSet :: (Set Int, Set Int) -> Set Int
intersectSet (set1, Set []) = Set []
intersectSet (Set [], set2) = Set []
intersectSet (Set (x:xs),set2) = if inSet x set2
  then insertSet x (intersectSet(Set xs, set2))
  else
    intersectSet(Set xs, set2)

--For this we have to backtrack as well once set1 has been exhausted
setDiff :: (Set Int, Set Int) -> Set Int
setDiff (set1, Set []) = set1
setDiff (Set [], set2) = set2
setDiff (Set (x:xs),set2) = if not (inSet x set2)
  then insertSet x (setDiff(Set xs, set2))
  else
    setDiff(Set xs, deleteSet x set2) --important to delete the element from set2

--Generating with [Int] -> [Int] gave me errors so I choose this approach
generateSetTupleQuickCheck :: ([Int],[Int]) -> (Set Int, Set Int)
generateSetTupleQuickCheck (a,b) = (list2set a, list2set b)

--Verify that every element in the unioned set is in at least one of the sets
test_set_union ::  (Set Int, Set Int) -> Set Int -> Bool
test_set_union (a, b) (Set []) = True
test_set_union (a, b) (Set (x:xs)) = (inSet x a || inSet x b) && test_set_union (a,b) (Set xs)

prop_set_union_in_one :: (Set Int, Set Int) -> Bool
prop_set_union_in_one (a, b) = test_set_union (a,b) (setUnion(a,b))

--Verify that every element in the intersected set is in both other sets
test_set_intersect ::  (Set Int, Set Int) -> Set Int -> Bool
test_set_intersect (a, b) (Set []) = True
test_set_intersect (a, b) (Set (x:xs)) = inSet x a && inSet x b && test_set_intersect (a,b) (Set xs)

prop_set_intersect_in_both :: (Set Int, Set Int) -> Bool
prop_set_intersect_in_both (a, b) = test_set_intersect (a,b) (intersectSet(a,b))

--Verify that every element in the setDiff-set is in either one set but not the other
test_set_diff ::  (Set Int, Set Int) -> Set Int -> Bool
test_set_diff (a, b) (Set []) = True
test_set_diff (a, b) (Set (x:xs)) = ((not (inSet x a) && inSet x b) || (inSet x a && not(inSet x b))) && test_set_diff (a,b) (Set xs)

prop_set_diff_in_either :: (Set Int, Set Int) -> Bool
prop_set_diff_in_either (a, b) = test_set_diff (a,b) (setDiff(a,b))

--(Sequence does [IO Int] -> IO [Int])
main :: IO ()
main = do
  putStrLn "===Excercise 1==="
  print =<< sequence (genList 10) -- Not sure how to convert this list into set
  --Spec does not mention if ordered is ascending or descending
  quickCheck (prop_set_ordered . generateSetQuickCheck)
  quickCheck (prop_set_no_duplicates . generateSetQuickCheck)
  putStrLn "===Excercise 2==="
  putStrLn "===Union tests==="
  --After union the regular properties of a set still apply
  quickCheck (prop_set_ordered . setUnion . generateSetTupleQuickCheck)
  quickCheck (prop_set_no_duplicates . setUnion . generateSetTupleQuickCheck)
  --Only additional specific property for union
  quickCheck (prop_set_union_in_one . generateSetTupleQuickCheck)
  --After union the regular properties of a set still apply
  putStrLn "===intersect tests==="
  quickCheck (prop_set_ordered . intersectSet . generateSetTupleQuickCheck)
  quickCheck (prop_set_no_duplicates . intersectSet . generateSetTupleQuickCheck)
  --Only additional specific property for intersect
  quickCheck (prop_set_intersect_in_both . generateSetTupleQuickCheck)
  putStrLn "===difference tests==="
  quickCheck (prop_set_ordered . setDiff . generateSetTupleQuickCheck)
  quickCheck (prop_set_no_duplicates . setDiff . generateSetTupleQuickCheck)
  quickCheck (prop_set_diff_in_either . generateSetTupleQuickCheck)
