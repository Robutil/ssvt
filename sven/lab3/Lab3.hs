
module Lab3 where

import Data.List
import System.Random
import Test.QuickCheck
import Lecture3

--Q1 1 hour
--There are also propositions that are always false such as (P && !P). (https://www.cs.odu.edu/~toida/nerzic/content/logic/prop_logic/tautology/tautology.html#:~:text=There%20are%20also%20propositions%20that,contradiction%20is%20called%20a%20contingency.)
contradiction :: Form -> Bool
contradiction f = all (\v -> not(evl v f)) (allVals f)
testContadiction = contradiction(Cnj [Prop 1, Neg(Prop 1)])

--A formula f is a tautology if it is satisfied by all valuations.
tautology :: Form -> Bool
tautology f = all (\v -> evl v f) (allVals f)
testTautology = tautology(Dsj [Prop 1, Neg(Prop 1)]) --Basic test P v not P
testTautology2 = tautology(Impl (Dsj [Prop 1, Prop 2]) (Dsj [Prop 1, Prop 2])) --Non minimal tautology ((A v B) -> (A v B)) (from wiki)

--B logically entails A is true if and only if all the valuations that satisfy B also satisfy A.
entails :: Form -> Form -> Bool
entails fa fb = all (\v -> if evl v fb then evl v fa else True) (allVals fb) --If fb evls to true, it should also for A, otherwise we dont have to check and can return A
testEntails = entails (Impl (Prop 1) (Prop 2)) (Impl (Neg(Prop 2)) (Neg(Prop 1))) --Entails can also be used for same as equiv

equiv :: Form -> Form -> Bool
equiv fa fb = all (\v ->  if evl v fb then evl v fa else True) (allVals fb) && all (\v ->  if evl v fa then evl v fb else True) (allVals fa)
testEquiv = equiv (Impl (Prop 1) (Prop 2)) (Impl (Neg(Prop 2)) (Neg(Prop 1))) -- Class example (a -> b) <-> (!b -> !a)

--Excercise 2
-- We are testing "parse :: String -> [Form]"
-- Grammar:
--   * == &&
--   + == ||
--   - == !
testParse = parse "*(1 +(2 -3))" /= []
testParse2 = parse "*(1 +(2 -3)" == []--missing bracket
testParse3 = parse "*(1 + *(2 -3))" == [] --adding conjunction in wrong place
testParse4 = parse "==> 1 2" == [] --Implicaiton the wrong way
testParse5 = parse "(1)==>(2)" == [] --Implicaiton the wrong way
testParse6 = parse "1==>2" /= [] --Goood....but parses as [1]
testParse7 = parse "(1==>2)" /= [] --Implicaiton the correct way simple example
testParse8 = parse "*(1 2)==>*(2 1)" /= [] --Good, but... this parses as [*(1 2)] which is incorrect
testParse9 = parse "(*(1 2)==>*(2 1))" /= [] --Good, more complex example which parses correctly

--Excercise 3
cnf :: Form -> Form
cnf f = nnf(arrowfree f)

--Form should be the same before and after arrowfree, using the same arrowFree functionailty is not the nicest thing tho
prop_cnf_arrowfree :: Form -> Bool
prop_cnf_arrowfree f = arrowfree (cnf f) == cnf f

prop_cnf_arrowfree_io :: IO Form -> IO Bool
prop_cnf_arrowfree_io f = do
  x <- f
  return $ arrowfree (cnf x) == cnf x

--Input form should be logically equiv to the form created by cnf
prop_cnf_equiv :: Form -> Bool
prop_cnf_equiv f = equiv f (cnf f)

--Only attoms are negated TODO seems like most work.. :P
prop_cnf_neg_atoms :: Form -> Bool
prop_cnf_neg_atoms f = True

--prop_repeatable doing it one 2 or 3 times yields same form
prop_cnf_repeatable :: Form -> Bool
prop_cnf_repeatable f = cnf f == cnf (cnf f) && cnf f == cnf(cnf(cnf f)) && cnf (cnf f) == cnf(cnf(cnf f))

--Excercise 4 multiple hours...atleast like 3-4
genZeroToSix :: Gen Int
genZeroToSix =
  choose(0,5)
  --getStdRandom (randomR(0,5))

-- The integer is the max recursive loop tries otherwise we get stuck infinitly
genFormElement :: Integer -> Gen Form
genFormElement n=
  if n == 0
    then
      do
      y <- genZeroToSix
      return $ Prop y
    else
      do
      x <- genZeroToSix
      y <- genZeroToSix
      formElement <- genFormElement (n-1)
      formElementTwo <- genFormElement (n-1)
      if x == 0
        then  return $ Prop y
        else if x == 1
          then return $ Neg (formElement)
          else if x == 2
            then  return $ Cnj [formElement, formElementTwo ]
            else if x == 3
              then return $ Dsj [formElement ,formElementTwo ]
              else if x == 4
                then return $ Impl (formElement) (formElementTwo)
                else if x == 5
                  then return $ Equiv (formElement  ) (formElementTwo )
                  else return $ Prop y



main :: IO ()
main = do
  putStrLn "===Excercise 1==="
  print testContadiction
  print testTautology
  print testTautology2
  print testEntails
  print testEquiv
  putStrLn "===Excercise 2==="
  quickCheck testParse
  quickCheck testParse2
  quickCheck testParse3
  quickCheck testParse4
  quickCheck testParse5
  quickCheck testParse6
  quickCheck testParse7
  quickCheck testParse8
  quickCheck testParse9
  putStrLn "===Excercise 3==="
  print (prop_cnf_arrowfree  (Equiv (Impl (Prop 1) (Prop 2))(Impl (Neg(Prop 2)) (Neg(Prop 1)))))
  print (prop_cnf_equiv (Equiv (Impl (Prop 1) (Prop 2))(Impl (Neg(Prop 2)) (Neg(Prop 1)))))
  print (prop_cnf_repeatable (Equiv (Impl (Prop 1) (Prop 2))(Impl (Neg(Prop 2)) (Neg(Prop 1)))))
  putStrLn "===Excercise 4==="
  quickCheck (forAll (genFormElement 10) prop_cnf_arrowfree)
  quickCheck (forAll (genFormElement 10) prop_cnf_repeatable)
  quickCheck (forAll (genFormElement 10) prop_cnf_equiv)
  --sample (genFormElement 10) for some examples
