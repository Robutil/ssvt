
module Lab5 where

import Data.List
import System.Random
import Test.QuickCheck
import Lecture5
import Debug.Trace
import Data.Time.Clock.POSIX (getPOSIXTime)

--Exercise 1 was placed in the Lecture5.hs file, was getting errors otherwise
gen_max_100 :: Gen (Integer, Integer, Integer)
gen_max_100 = do
 x <- choose(1,1000000)
 y <- choose(1,1000000)
 z <- choose(1,1000000)
 return (x,y,z)

prop_exM_expM_equal :: (Integer, Integer, Integer) -> Bool
prop_exM_expM_equal (x,y,modulo)= exM x y modulo == expM x y modulo

--Use these to seperately run the tests, they should always resolve to true
-- Somehow these 2 tests dont look to actually be ran since they both take 0 time
test_exM :: (Integer, Integer, Integer) -> Bool
test_exM (x,y,modulo)= exM x y modulo >= 0

test_expM :: (Integer, Integer, Integer) -> Bool
test_expM (x,y,modulo)= exM x y modulo >= exM x y modulo

--Exercise 4
--Lots of information on the subjet gathered from https://youtu.be/jbiaz_aHHUQ
carmichael :: [Integer]
carmichael = [ (6*k+1)*(12*k+1)*(18*k+1) |
  k <- [2..],
  prime (6*k+1),
  prime (12*k+1),
  prime (18*k+1) ]

test_carmichael_fermat :: [Integer] -> IO Bool
test_carmichael_fermat [] = return True
test_carmichael_fermat (x:xs) = do
  primeTest <- primeTestF x
  nextTest <- test_carmichael_fermat xs
  if primeTest && nextTest
    then return True
    else
      return False

--Q2 Ex 4
--All of the carmichael numbers are composite primes instead of really primes.
--What they have in common is that they trick fermats theorem.
--Only 2183 numbers out of 25 billion numbers fail this test (just using fermats with 2^p - 2)
--Meaning fermats theorem is still a good quick way to check but offers only 99.9999999% accuracy not 100%.

--Q3 Ex 4
test_carmichael_miller_rabin' :: (Integer, Integer) -> IO Bool
test_carmichael_miller_rabin' (k,n) = primeMR (fromIntegral k) n

test_carmichael_miller_rabin :: [Integer] -> IO Bool
test_carmichael_miller_rabin [] = return True
test_carmichael_miller_rabin (x:xs) = do
  mrTest <- test_carmichael_miller_rabin' (decomp(x-1))
  nextMrTest <- test_carmichael_miller_rabin xs
  if not mrTest && not nextMrTest
    then return True
    else
      return False

--Excercise 7
--Found some inspiration here; https://medium.com/@prudywsh/how-to-generate-big-prime-numbers-miller-rabin-49e6e6af32fb
--randomly generate a large number of a bit length
--Test if its prime
generate256BitInt :: IO Integer
generate256BitInt = getStdRandom (randomR((2 ^ 256)+1, (2^257)-1))

test_miller_rabin :: (Integer, Integer) -> IO Bool
test_miller_rabin (k,n) = primeMR (fromIntegral k) n

genBigPrime :: IO Integer
genBigPrime = do
  x <- generate256BitInt
  if even x --Discard even numbers instantly
    then genBigPrime
    else
      do
        passesFermat <- primeTestF x
        passesMr <- test_miller_rabin (decomp(x-1))
        if passesFermat && passesMr
          then return x
          else
            genBigPrime

--Generate a second prime but ensure its not equal to the first
genBigPrime2 :: Integer -> IO Integer
genBigPrime2 n = do
  x <- generate256BitInt
  if even x || x == n --If even or equal to prev prime, discard and retry
    then genBigPrime2 n
    else
      do
        passesFermat <- primeTestF x
        passesMr <- test_miller_rabin (decomp(x-1))
        if passesFermat && passesMr
          then return x
          else
            genBigPrime2 n

genRsaPair :: IO (Integer, Integer)
genRsaPair = do
  x <- genBigPrime
  y <- genBigPrime2 x
  return (x,y)

main :: IO ()
main = do
  putStrLn "===Excercise 1==="
  quickCheck (forAll gen_max_100 prop_exM_expM_equal)
  quickCheck (forAll gen_max_100 test_exM)
  quickCheck (forAll gen_max_100 test_expM)
  --Tested with expM 10000 9999121233 12342,
  --this takes <1 sec with our new function and runs out of memory with the old function
  --Removing two factor of exponent and running expM 10000 99991212 12342
  --takes <1 second with the new function, and over a minute with the old one
  putStrLn "===Excercise 4==="
  putStrLn "Testing if carmichael numbers pass fermats..."
  print =<< test_carmichael_fermat (take 1000 carmichael)
  putStrLn "First 1000 carmichael numbers pass the fermats test"
  putStrLn "==="
  putStrLn "Testing if carmichael numbers fail miller rabins test..."
  print =<< test_carmichael_miller_rabin (take 1000 carmichael)
  putStrLn "First 1000 carmichael numbers fail the miller rabins test"
  putStrLn "===Excercise 7==="
  print =<< genRsaPair
