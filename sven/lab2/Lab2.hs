
module Lab2 where

import Data.List
import Data.Char
import System.Random
import Test.QuickCheck
import Debug.Trace

infix 1 -->

(-->) :: Bool -> Bool -> Bool
p --> q = (not p) || q

forall :: [a] -> (a -> Bool) -> Bool
forall = flip all

probs :: Int -> IO [Float]
probs 0 = return []
probs n = do
             p <- getStdRandom random
             ps <- probs (n-1)
             return (p:ps)

data Shape = NoTriangle | Equilateral
           | Isosceles  | Rectangular | Other deriving (Eq,Show)




--Assignment 1
checkQuarts :: Int -> Bool
checkQuarts n = n >= 2000 && n <= 3000

probsSplit :: Float -> Float -> [Float] -> [Float]
probsSplit n m [] = []
probsSplit n m (x:xs) = if( x>=n &&x<m )
  then [x] ++ probsSplit n m xs
  else []  ++  probsSplit n m xs

testProbs :: Int -> IO Bool
testProbs n = do
  probablityList <- probs n
  let quartsPartOne = probsSplit 0.0 0.25 probablityList
  let quartsPartTwo = probsSplit 0.25 0.5 probablityList
  let quartsPartThree = probsSplit 0.5 0.75 probablityList
  let quartsPartFour = probsSplit 0.75 1.0 probablityList
  return $ (checkQuarts (length quartsPartOne) && checkQuarts (length quartsPartTwo) && checkQuarts (length quartsPartThree)  && checkQuarts (length quartsPartFour))

testProbsMultiple :: Int -> IO Bool
testProbsMultiple 0 = return $ True
testProbsMultiple n = do
  probTest <- testProbs 10000
  if probTest
    then testProbsMultiple(n-1)
    else return $ False
--Sometimes when running the test 10 times it fails (about 1/5).
--This is with a margin of 2400-2600 instead of 2500 for a quart.
--Depending on the margin I would say it is random enoug

-- Excersise 2
-- NoTriangle check from : https://www.wikihow.com/Determine-if-Three-Side-Lengths-Are-a-Triangle
triangle :: Integer -> Integer -> Integer -> Shape
triangle a b c
  | not(a+b>c && a+c>b && b+c>a) = NoTriangle
  | a==b && b==c = Equilateral
  | a^2+b^2==c^2 || a^2+c^2==b^2 ||  b^2+c^2==a^2 = Rectangular
  | a==b || a == c || b == c = Isosceles
  | otherwise =Other

-- To test, generate psuedo random tuples according to the constraints of the tested type
genEquilateral :: IO Integer
genEquilateral = getStdRandom (randomR(0,100000))

testEquilateral :: IO Bool
testEquilateral = do
  a <- genEquilateral
  return (triangle a a a == Equilateral)

testEquilateralMult :: Integer -> IO Bool
testEquilateralMult 0 = return True
testEquilateralMult n = do
  result <- testEquilateralMult(n-1)
  resultCall <- testEquilateral
  return $ resultCall && result

--Q3a

prop1 :: Integer -> Bool
prop1 n = n `mod` 2 == 0 && n > 3

prop2 :: Integer -> Bool
prop2 n = n `mod` 2 == 0

prop3 :: Integer -> Bool
prop3 n = n `mod` 2 == 0 || n > 3

prop4 :: Integer -> Bool
prop4 n = (n `mod` 2 == 0 && n > 3) || n `mod` 2 == 0
--All the evens are prop 2, the 3rd left and 4th right are the same; consolidated in prop4 func

--Q3b
stronger, weaker :: [a] -> (a -> Bool) -> (a -> Bool) -> Bool
stronger xs p q = forall xs (\ x -> p x --> q x)
weaker   xs p q = stronger xs q p

quicksortProp :: [(a -> Bool)] -> [a] -> [(a -> Bool)]
quicksortProp [] _ = []
quicksortProp (x:xs) ys= quicksortProp [ a | a <- xs, stronger ys a x ] ys
   ++ [x]
   ++ quicksortProp [ a | a <- xs, weaker ys a x ] ys

testPropSort :: [Bool]
testPropSort =  do
  sortedList <- quicksortProp [prop1, prop2, prop3, prop4] [-10..10]
  let sortedlistHead = head [sortedList]
  return True

--Exercise 4
-- If both lists are exhausted, they are permutations of eachother
-- If one gets exhausted faster, its not a permutation
quickSort :: Ord a => [a] -> [a]
quickSort [] = []
quickSort (x:xs) =
   quickSort [ a | a <- xs, a <= x ]
   ++ [x]
   ++ quickSort [ a | a <- xs, a > x ]

isPermutationChecker :: Ord a => [a] -> [a] -> Bool
isPermutationChecker [] [] = True
isPermutationChecker (x:xs) [] = False
isPermutationChecker []  (y:ys) = False
isPermutationChecker (x:xs) (y:ys) =   x == y && isPermutationChecker xs ys

-- Lab 2 Question 4
isPermutation :: Eq a => [a] -> [a] -> Bool
isPermutation xs ys = xs `elem` (permutations ys) -- Very slow; better to recursively take elem and cmp

--This test should always return true
testPermutationsTrue :: [Integer] -> Bool
testPermutationsTrue xs = isPermutation xs xs
-- Always append element 5, should always return false, check that it does NOT return true
-- Adding this test led me to finding and fixing "Lab2.hs:(146,1)-(147,87): Non-exhaustive patterns in function isPermutation"
testPermutationsFalse :: [Integer] -> Bool
testPermutationsFalse xs = not (isPermutation xs (xs++[5]))

testPermutationsFalse2 :: [Integer] -> Bool
testPermutationsFalse2 xs = not (isPermutation xs (xs++[head xs]))

incrementIndex :: Int -> [Integer] -> [Integer]
incrementIndex n xs = take n xs ++ ((xs !! n) + 1) : drop (n+1) xs

-- Test data (no duplicates)
permTestData = [-4..4] -- Base case
permTestData1 = -5 : permTestData
permTestData2 = permTestData ++ [5]
permTestData3 = incrementIndex 2 permTestData
permTestData4 = drop 1 permTestData
permTestData5 = permutations permTestData !! 250 -- Valid case
permTestData6 :: [Integer]
permTestData6 = []

--Assignment 5
isDerangement :: Eq b => [b] -> [b] -> Bool
isDerangement [] [] = False
isDerangement [] ys = False
isDerangement xs [] = False
isDerangement xs ys = isPermutation xs ys && (length [x | x <- zip xs ys, fst x == snd x]) == 0

deran :: (Eq t, Num t, Enum t) => t -> [[t]]
deran n = let a = [0..n] in [x | x <- permutations a, isDerangement x a]

deranTest1 = isDerangement permTestData permTestData -- Invalid
-- ...
deranTest6 = isDerangement permTestData permTestData1 -- Invalid
deranTestN = all (==True) [isDerangement x [0..5] | x <- deran 5] -- Valid, but spooky because we use the same function to generate

-- All properties of Question 4 should hold because a derangement is a permutation and derangement is also stronger
-- Some test cases may fail however.
-- The other property is that no index in a deran list can have the same value as the input list. This is already
-- checked by the zip function. The best way to test this is to have a different implementation. TODO: this. Replacing
-- the zip implementation will also freeup deran to be used in test generation.

-- Test case; isDerangement always returns false when it gets passed the same list twice
--Precond is isTrue for any input where the same list is used
isTrue :: a-> Bool
isTrue _ = True
--Postcond isFalse
isFalse :: a-> Bool
isFalse _ = False

testDeran :: Eq b => [b] -> Bool
testDeran xs = isDerangement xs xs

main :: IO ()
main = do
  putStrLn "===Excercise 1==="
  quickCheck =<< testProbsMultiple 10
  putStrLn "===Excercise 2==="
  quickCheck =<<  testEquilateralMult 100
  putStrLn "===Excercise 3b (not sure how to print result though)==="
  print testPropSort
  putStrLn "===Excercise 4==="
  quickCheck testPermutationsTrue
  quickCheck testPermutationsFalse
  quickCheck testPermutationsFalse2
  putStrLn "===Excercise 5==="
  --quickCheck(isFalse . testDeran)
