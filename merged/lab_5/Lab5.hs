module Lab5 where

import Data.List
import System.Random
import Test.QuickCheck
import Lecture5

-- Lab 5 assignment Group 3
-- By: Jelle Sinnige, Robbin Siepman, Sven Keunen, Jeffrey Chong

-- Helper functions are put in lab_5/Lecture5.hs

-- Excercise 1 (Time indication: -3 hours)

-- recursively solve the problem
-- wikipedia gives us an iterative solution
-- we can reverse that into a recursive one
-- step 1 -> set result one, and a variable x to a
-- if the last bit of b == 1 ( or b modulo 2 == 1 ), then:
--      multiply the result with x, and modulo it by c
--      divide b by 2
-- square x
-- repeat while b =/= 0



-- Implementation and comments on the function can be found in Lecture5.hs exM
-- that implementation is repeated here, so it won't be accidentally overlooked
exM' :: Integer -> Integer -> Integer -> Integer
exM' _ 0 _ = 1
exM' 1 _ _ = 1
exM' a b c
    | (b `mod` 2) == 1 = (a * exM' a (b `div` 2) c^2) `mod` c
    | otherwise        = (exM' a (b `div` 2) c^2) `mod` c



genTupleExm :: Gen (Integer, Integer, Integer)
genTupleExm = do
 x <- choose(1,1000000)
 y <- choose(1,1000000)
 z <- choose(1,1000000)
 return (x,y,z)

prop_exM_expM_equal :: (Integer, Integer, Integer) -> Bool
prop_exM_expM_equal (x,y,modulo)= exM x y modulo == expM x y modulo

--Use these to seperately run the tests, they should always resolve to true
test_exM :: (Integer, Integer, Integer) -> Bool
test_exM (x,y,modulo)= exM x y modulo >= 0

test_expM :: (Integer, Integer, Integer) -> Bool
test_expM (x,y,modulo)= expM x y modulo >= 0

--Besides the automated tests we did some manual speed comparisons
-- Speed comparisons
-- (6^12345) `mod` 5           -- (0.01 secs, 79,176 bytes)
-- exM 6 12345 5               -- (0.01 secs, 87,560 bytes)
--
-- (6^123456789) `mod` 5       -- (3.25 secs, 111,831,472 bytes)
-- exM 6 123456789 5           -- (0.01 secs, 124,248 bytes)
--
-- expM 10000 9999121233 12342 -- Runs out of memory
-- exM 10000 9999121233 12342  -- <1 second
--
-- Removing two factor of exponent and running
-- expM 10000 99991212 12342   --Takes over a minute
-- exM 10000 99991212 12342    --Takes <1 second

-- Lab 5 question 2
-- This research https://www.ams.org/journals/mcom/1989-53-187/S0025-5718-1989-0969484-8/S0025-5718-1989-0969484-8.pdf
-- shows a way to generate carmichael numbers, which are the bane of Fermat's Primality Check. The method consists of
-- the multiplication of three primes with certain properties. However, to not only generate False postives we can just
-- multiply three primes together. We can be more efficient if we generate subsequences of the first n primes.
getComposite n = product (drop n (take (n+3) primes))
composites = map getComposite [0..] -- Generates infinite numbers

-- Doesn't generate infinite numbers, but more likely to generate Carmichael numbers
composites' n = sort [product x | x <- subsequences (take n primes), length x > 1, length x < 5]

testFermats :: [Integer] -> IO ()
testFermats [] = print "All tests passed"
testFermats (x:xs) = do
    garbage <- primeTestF x
    if garbage then print (garbage, x) else testFermats xs

-- Lab 5 question 3
foolFermats [] = print "Unable to fool"
foolFermats (x:xs) = do
    t1 <- primeTestsF 1 x
    t2 <- primeTestsF 2 x
    t3 <- primeTestsF 3 x
    if t1 && t2 && t3 then print x else foolFermats xs
-- foolFermats (composites' 20) -- 91 (lowest found thusfar)

-- primeTestsF 1 101 -- True
-- primeTestsF 3 101 -- True
{-
 - The `k` parameter is the amount of random tests executed.
 -
 - I am not sure how to efficiently do an fmap of IO Bool, hence the horrible
 - t1,t2,t3 bindings. The results are very haphazard and unpredictable, as such
 - is the case with random testing.
 -}

--Exercise 4 Time indication; 3 hours
--Lots of information on the subject gathered from https://youtu.be/jbiaz_aHHUQ
carmichael :: [Integer]
carmichael = [ (6*k+1)*(12*k+1)*(18*k+1) |
  k <- [2..],
  prime (6*k+1),
  prime (12*k+1),
  prime (18*k+1) ]

test_carmichael_fermat :: [Integer] -> IO Bool
test_carmichael_fermat [] = return True
test_carmichael_fermat (x:xs) = do
  primeTest <- primeTestF x
  nextTest <- test_carmichael_fermat xs
  if primeTest && nextTest
    then return True
    else
      return False

--Q2 Ex 4
--All of the carmichael numbers are composite primes instead of really primes.
--What they have in common is that they trick fermats theorem.
--Only 2183 numbers out of 25 billion numbers fail this test (just using fermats with 2^p - 2)
--Meaning fermats theorem is still a good quick way to check but offers only 99.9999999% accuracy not 100%.

--Q3 Ex 4
test_carmichael_miller_rabin' :: (Integer, Integer) -> IO Bool
test_carmichael_miller_rabin' (k,n) = primeMR (fromIntegral k) n

test_carmichael_miller_rabin :: [Integer] -> IO Bool
test_carmichael_miller_rabin [] = return True
test_carmichael_miller_rabin (x:xs) = do
  mrTest <- test_carmichael_miller_rabin' (decomp(x-1))
  nextMrTest <- test_carmichael_miller_rabin xs
  if not mrTest && not nextMrTest
    then return True
    else
      return False

-- Exercise 5
mersenne x = do
    checkPrimeMR <- primeMR 1 x
    if checkPrimeMR then return x else return 0

generateMersennes = do
    let probablePrimes = [2^p-1 | p <- take 8 primes]
    mersenneMapped <- mapM mersenne probablePrimes
    return (filter (/= 0) mersenneMapped)

isqrt x = floor (sqrt (fromIntegral  x))
isPrime :: Integer -> Bool
isPrime k = if k > 1 then null [ x | x <- [2..isqrt k], k `mod` x == 0] else False -- Stolen from StackOverflow
generateMersennesTest = do
    tests <- generateMersennes
    let results = map isPrime tests
    return (all (==True) results)

-- The generated Mersenne primes are only as strong as the primeMR function.
-- If you are unlucky, it will generate false positives

-- Exercise 6
tree1 n = grow (step1 n) (1,1)
step1 n = \ (x,y) -> if x+y <= n then [(x+y,x),(x,x+y)] else [] -- step function

tree2 n = grow (step2 n) (1,1)
step2 n = \ (x,y) -> if x+y <= n then [(x+y,y),(x,x+y)] else [] -- step function

explodeTree (T x []) = [x]
explodeTree (T x xs) = x : concat [explodeTree y | y <- xs]

generateCoprimeSets 0 = [(1,1)]
generateCoprimeSets n = [(x,y) | x <- [1..n], y <- [1..n], coprime x y]

testCoprimeTree1 x = let n = abs x in sort (explodeTree (tree1 n)) == sort (generateCoprimeSets n)
testCoprimeTree2 x = let n = abs x in sort (explodeTree (tree2 n)) == sort (generateCoprimeSets n)

--Excercise 7 Time indication; 3 hours
--Found some inspiration here; https://medium.com/@prudywsh/how-to-generate-big-prime-numbers-miller-rabin-49e6e6af32fb
--randomly generate a large number of a bit length
--Keep on going untill we find a number that passes the checks
generate256BitInt :: IO Integer
generate256BitInt = getStdRandom (randomR((2 ^ 256)+1, (2^257)-1))

test_miller_rabin :: (Integer, Integer) -> IO Bool
test_miller_rabin (k,n) = primeMR (fromIntegral k) n

genBigPrime :: IO Integer
genBigPrime = do
  x <- generate256BitInt
  if even x --Discard even numbers instantly
    then genBigPrime
    else
      do
        passesFermat <- primeTestF x
        passesMr <- test_miller_rabin (decomp(x-1))
        if passesFermat && passesMr
          then return x
          else
            genBigPrime

--Generate a second prime but ensure its not equal to the first
genBigPrime2 :: Integer -> IO Integer
genBigPrime2 n = do
  x <- generate256BitInt
  if even x || x == n --If even or equal to prev prime, discard and retry
    then genBigPrime2 n
    else
      do
        passesFermat <- primeTestF x
        passesMr <- test_miller_rabin (decomp(x-1))
        if passesFermat && passesMr
          then return x
          else
            genBigPrime2 n

genRsaPair :: IO (Integer, Integer)
genRsaPair = do
  x <- genBigPrime
  y <- genBigPrime2 x
  return (x,y)

-- From https://en.wikipedia.org/wiki/RSA_(cryptosystem)
-- 1 The generated pair is the secret Q and P
-- 2 From this n is computed using n = pq, this is used as the modulus for the public and private key
--    n is released is part of the public key
-- 3 "Compute λ(n), where λ is Carmichael's totient function. Since n = pq, λ(n) = lcm(λ(p),λ(q)), and since p and q are prime, λ(p) = φ(p) = p − 1 and likewise λ(q) = q − 1. Hence λ(n) = lcm(p − 1, q − 1).
-- λ(n) is kept secret."
-- 4 "Choose an integer e such that 1 < e < λ(n) and gcd(e, λ(n)) = 1; that is, e and λ(n) are coprime."
--    e is released as part of the public key
-- 5 "Determine d as d ≡ e−1 (mod λ(n)); that is, d is the modular multiplicative inverse of e modulo λ(n)."
--    d is kept secret as the private key component

main :: IO ()
main = do
  putStrLn "===Excercise 1==="
  quickCheck (forAll genTupleExm prop_exM_expM_equal)
  putStrLn "Testing slow verion expM"
  quickCheck (forAll genTupleExm test_expM)
  putStrLn "Testing fast verion exM"
  quickCheck (forAll genTupleExm test_exM)
  putStrLn "===Exercise 2==="
  testFermats (sort (take 20 composites))
  putStrLn "===Exercise 3==="
  foolFermats (composites' 20)
  putStrLn "===Excercise 4==="
  putStrLn "Testing if carmichael numbers pass fermats..."
  print =<< test_carmichael_fermat (take 1000 carmichael)
  putStrLn "First 1000 carmichael numbers pass the fermats test"
  putStrLn "==="
  putStrLn "Testing if carmichael numbers fail miller rabins test..."
  print =<< test_carmichael_miller_rabin (take 1000 carmichael)
  putStrLn "First 1000 carmichael numbers fail the miller rabins test"
  putStrLn "===Exercise 5==="
  gmt1 <- generateMersennesTest
  print gmt1
  putStrLn "===Exercise 6==="
  quickCheck testCoprimeTree1
  quickCheck testCoprimeTree2
  putStrLn "===Excercise 7==="
  print =<< genRsaPair
