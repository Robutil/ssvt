import System.Random
import Data.Function
import Data.List
import Data.Maybe
import Data.Char
import Test.QuickCheck

infix 1 -->
(-->) :: Bool -> Bool -> Bool
p --> q = (not p) || q
forall = flip all

-- Lab 2 Question 1
probs :: Int -> IO [Float]
probs 0 = return []
probs n = do
        p <- getStdRandom random
        ps <- probs (n-1)
        return (p:ps) 

{- 
 - Increments Integer of index n by 1
 - @param n: Index to increment
 - @param xs: List of integers of at least length n
 - @return Updates list of integers, with one value incremented
-}
incrementIndex :: Int -> [Integer] -> [Integer]
incrementIndex n xs = take n xs ++ ((xs !! n) + 1) : drop (n+1) xs 

{-
 - Calculates the percentile index for x based on the number of percentiles n
 - E.g. 4 0.1 will be mapped to 0 ([here, not, not, not])
 - @param n: The number of percentiles
 - @param x: A float that is part of one of the percentiles (range 0-1)
 - @return Index within range of 0 to n (not including n)
-}
percentileIndex :: Int -> Float -> Int
percentileIndex n x = floor (x / (1.0 / fromIntegral n))

{-
 - Calculates how many occurences there are of each percentiles
 - @param (x:xs): List of values to classify (each must range from 0-1)
 - @param n: How many percentiles there are
 - @param percentiles: Current count of each percentile (first time should be `replicate n 0`)
 - @return An updated list of percentiles where each value is counted
-}
updatePercentiles :: [Float] -> Int -> [Integer] -> [Integer]
updatePercentiles [] n percentiles = percentiles
updatePercentiles (x:xs) n percentiles = updatePercentiles xs n (incrementIndex (percentileIndex n x) percentiles)

{-
 - Counts the numbers in each quartile for the given input
 - @param xs: Input numbers between the range of 0-1
 - @return Number of occurences in each quartile
-}
countQuartiles :: [Float] -> [Integer]
countQuartiles xs = updatePercentiles xs 4 (replicate 4 0)

quartilePremise :: Integer -> Bool
quartilePremise x = x > 2400 && x < 2600 -- Depending on how lucky we are, tests can fail

-- The idea for the above implementation is that we can handle any arbitrary number of percentiles. This can also
-- open up avenues for different tests. The only constants we have to change are the ones belonging to quartiles,
-- in this case the number of percentiles, 4, and the premise (range 2400-2600)
quartileTest :: IO Bool
quartileTest = do
    probabilities <- probs 10000
    let quartiles = countQuartiles probabilities
    return $ all (==True) [quartilePremise x | x <- quartiles]
-- For QuickCheck the property is prop_QuartilePremise x = quartilePremise
-- But I dont know how to link probs for input

-- Lab 2 Question 2
data Shape = NoTriangle | Equilateral | Isosceles | Rectangular | Other deriving (Eq,Show)
sortedTriangleSides :: Integer -> Integer -> Integer -> [Integer]
sortedTriangleSides x y z = sort [x,y,z]

isNoTriangle :: Integer -> Integer -> Integer -> Bool
isNoTriangle x y z = let a = sortedTriangleSides x y z in sum (take 2 a) < last a

isEquilateral :: Integer -> Integer -> Integer -> Bool
isEquilateral x y z = x == y && y == z && x == z

isRectangular :: Integer -> Integer -> Integer -> Bool
isRectangular x y z = let a = sortedTriangleSides x y z in ((a!!0)^2 + (a!!1)^2)==(a!!2)^2

isIsosceles :: Integer -> Integer -> Integer -> Bool
isIsosceles x y z = let a = sortedTriangleSides x y z in a !! 0 == a !! 1

triangle :: Integer -> Integer -> Integer -> Shape
triangle x y z
    | isNoTriangle x y z = NoTriangle
    | isEquilateral x y z = Equilateral
    | isRectangular x y z = Rectangular
    | isIsosceles x y z = Isosceles
    | otherwise = Other
    
-- Testing would be done by generating triangles and checking the Shape
-- E.g. for equilaterals = [(x, x, x) | x <- numberOfTests]

genEquilateral :: IO Integer
genEquilateral = getStdRandom (randomR(0,100000))

testEquilateral :: IO Bool
testEquilateral = do
  a <- genEquilateral
  return (triangle a a a == Equilateral)

testEquilateralMult :: Integer -> IO Bool
testEquilateralMult 0 = return True
testEquilateralMult n = do
  result <- testEquilateralMult(n-1)
  resultCall <- testEquilateral
  return $ resultCall && result




-- Lab 2 Question 3
stronger, weaker :: [a] -> (a -> Bool) -> (a -> Bool) -> Bool
stronger xs p q = forall xs (\ x -> p x --> q x)
weaker   xs p q = stronger xs q p 

properties = [
    (even),
    (\x -> even x && x > 3),
    (\x -> even x || x > 3),
    (\x -> (even x && x > 3) || even x)]

isStronger p1 p2 = stronger [-10..10] p1 p2 

calcPropertyStr :: (Enum a, Num a) => (a -> Bool) -> [a -> Bool] -> Int
calcPropertyStr prop others = length [x | x <- others, isStronger prop x]

-- TODO: map scores to human readable property
calcPropertyScores = map (\x -> calcPropertyStr x properties) properties


-- Lab 2 Question 4
isPermutation :: Eq a => [a] -> [a] -> Bool
isPermutation xs ys = xs `elem` (permutations ys) -- Very slow; better to recursively take elem and cmp

-- They can only be permutations if they have the same length
isPermutationProperty1 :: Eq a => [a] -> [a] -> Bool
isPermutationProperty1 xs ys = length xs == length ys
 
-- All items from list xs must be in ys
isPermutationProperty2 :: Eq a => [a] -> [a] -> Bool
isPermutationProperty2 xs ys = all (==True) [x `elem` ys | x <- xs] 

-- All items from list ys must be in xs
isPermutationProperty3 :: Eq a => [a] -> [a] -> Bool
isPermutationProperty3 xs ys = all (==True) [x `elem` xs | x <- ys] 

-- strength of prop 2 and 3 are equal (both Stronger)
-- prop 1 is stronger than 2 and 3

-- Test data (no duplicates)
permTestData = [-4..4] -- Base case
permTestData1 = -5 : permTestData
permTestData2 = permTestData ++ [5]
permTestData3 = incrementIndex 2 permTestData
permTestData4 = drop 1 permTestData
permTestData5 = permutations permTestData !! 250 -- Valid case
permTestData6 :: [Integer]
permTestData6 = [] 
-- Maybe reverse, etc

permTest1 = isPermutation permTestData permTestData -- Valid
permTest2 = isPermutation permTestData permTestData1 -- Invalid
permTest3 = isPermutation permTestData permTestData2 -- Invalid
permTest4 = isPermutation permTestData permTestData3 -- Invalid
permTest5 = isPermutation permTestData permTestData4 -- Invalid
permTest6 = isPermutation permTestData permTestData5 -- Valid
permTest7 = isPermutation permTestData permTestData6 -- Invalid
permTest8 = isPermutation permTestData6 permTestData6 -- Valid

-- TODO: Ordered list of properties using strength and weaker
-- TODO: Automate the test process using QuickCheck (use properties to filter our valid test cases)


-- Lab 2 Question 5
isDerangement :: Eq b => [b] -> [b] -> Bool
isDerangement xs ys = isPermutation xs ys && (length [x | x <- zip xs ys, fst x == snd x]) == 0

deran :: (Eq t, Num t, Enum t) => t -> [[t]]
deran n = let a = [0..n] in [x | x <- permutations a, isDerangement x a]

deranTest1 = isDerangement permTestData permTestData
deranTest2 = isDerangement permTestData permTestData1
deranTest3 = isDerangement permTestData permTestData2
deranTest4 = isDerangement permTestData permTestData3
deranTest5 = isDerangement permTestData permTestData4
deranTest6 = isDerangement permTestData permTestData5
deranTest7 = isDerangement permTestData permTestData6
deranTest8 = isDerangement permTestData6 permTestData6
deranTestN = all (==True) [isDerangement x [0..5] | x <- deran 5] -- Valid, but spooky because we use the same function to generate

-- If a derangement holds for an input, it also holds for the input the otherway around, same for false
prop_Reversable :: [Int] -> [Int] -> Bool
prop_Reversable xs ys = isDerangement xs ys == isDerangement ys xs

--Can be tested with "sample genListSize"
genListSize :: Gen Int
genListSize =
  choose (1, 100)



-- All properties of Question 4 should hold because a derangement is a permutation and derangement is also stronger
-- Some test cases may fail however.
-- The other property is that no index in a deran list can have the same value as the input list. This is already
-- checked by the zip function. The best way to test this is to have a different implementation. TODO: this. Replacing
-- the zip implementation will also freeup deran to be used in test generation.


-- Lab 2 Question 6
-- roti encoding is a weak cryptographic cypher that increments an ascii value by 13 (within roti bounds)
-- which makes it symmetric; the key and value can both be found with the same roti call.
rotiCypherBase = ['A'..'Z'] ++ ['a'..'z']
rotiCypherCryp = ['N'..'Z'] ++ ['A'..'M'] ++ ['n'..'z'] ++ ['a'..'m']
rotiChar a = if a `elem` rotiCypherBase then rotChar a else a
rotChar a = rotiCypherCryp !! (fromJust $ elemIndex a rotiCypherBase)
roti someString = [rotiChar c | c <- someString]

rotiCheck :: [Char] -> Bool
rotiCheck someString = (roti(roti(someString))==someString) && length someString == length (roti someString)
-- We could make more relevant cases if we force input from rotiCypherBase
-- Then we could do things like 
--  - isDerangement (roti someString) someString 

-- Below are the same tests implemented as QuickCheck properties
prop_KeepLength xs = length (roti xs) == length xs
    where types = xs::[Char]

prop_Symmetric xs = roti (roti xs) == xs
    where types = xs::[Char]

-- Lab 2 Question 7
-- assert length and country code
--getRawValue -> char to int unless not int then char - 'A' + 10
getRawValue x = if (isAlpha x) then (ord x) - (ord 'A') + 10 else read [x] -- Invalid iban characters are handled here
rotateIban iban = (drop 4 iban) ++ (take 4 iban)

-- Reads the list of integers as a single number
mergeIbanDigits :: [Int] -> String -> Integer
mergeIbanDigits [] n = read n
mergeIbanDigits (x:xs) n = mergeIbanDigits xs (n ++ show x)

-- Checks whether the given iban is a valid number
validateIban :: String -> Bool
validateIban iban = (mergeIbanDigits (map getRawValue (rotateIban iban)) "" ) `mod` 97 == 1 && length iban < 34
-- We do not do any country specific length lookups, but this can be achieved by something like:
-- validLength iban = countryLookupTable !! fromInteger $ mergeIbanDigits (take 2 iban) "" == length iban
-- where countryLookupTable is a list of lenghts corresponding with the country codes  

validateIbanTest = validateIban "GB82WEST12345698765432" -- Valid
validateIbanTest1 = validateIban "AL35202111090000000001234567" -- Valid
validateIbanTest2 = validateIban "AD1400080001001234567890" -- Valid
validateIbanTest3 = validateIban "NL02ABNA0123456789" -- Valid
validateIbanTest4 = validateIban "NL02ABNA0223456789" -- Invalid (Changed a number)
-- The test generation could be automated because there is a way to generate iban numbers, as depected by wikipedia.
-- However the generation procedure would be very similar to the actual implementation, so essentially you would be 
-- testing if one implementation is similar to another (which is why it is not implemented here)

-- Something like this may be interesting, if we generate valid AlphaNumeric
-- prop_QuickCheckCannotGuessIban xs = not $ validateIban xs
--     where types = xs::[Char]

main :: IO ()
main = do
    putStrLn "===Excercise 1==="
    quartileTestResult <- quartileTest
    print quartileTestResult
    putStrLn "===Excercise 2==="
    print (triangle 26 10 24 == Rectangular) 
    quickCheck =<<  testEquilateralMult 100
    putStrLn "===Excercise 3==="
    putStrLn "Shows strength for property in order"
    print (calcPropertyScores)
    putStrLn "===Excercise 4==="
    print (permTest1)
    print (not permTest2)
    print (not permTest3)
    print (not permTest4)
    print (not permTest5)
    print (permTest6)
    print (not permTest7)
    print (permTest8)
    putStrLn "===Excercise 5==="
    print (not deranTest1)
    print (not deranTest2)
    print (not deranTest3)
    print (not deranTest4)
    print (not deranTest5)
    print (not deranTest6)
    print (not deranTest7)
    print (deranTest8)
    print (deranTestN)
    --quickCheck prop_Reversable not sure how to make generator which limits amount of ints yet
    putStrLn "===Excercise 6==="
    quickCheck rotiCheck
    quickCheck prop_KeepLength
    quickCheck prop_Symmetric
    putStrLn "===Excercise 7==="
    print (validateIbanTest)
    print (validateIbanTest1)
    print (validateIbanTest2)
    print (validateIbanTest3)
    print (not validateIbanTest4)

