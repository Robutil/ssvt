-- Euler problem 6 - Sum square difference
-- https://projecteuler.net/problem=6
sumSquares :: Integer -> Integer
sumSquares n = sum [x^2 | x <- [1..n]]

squareNatural :: Integer -> Integer
squareNatural n = (^2) $ sum [1..n]

diffSumSquare :: Integer -> Integer
diffSumSquare n = squareNatural n - sumSquares n

euler6 :: Integer
euler6 = diffSumSquare 100

-- Euler problem 8
bigNumber = 7316717653133062491922511967442657474235534919493496983520312774506326239578318016984801869478851843858615607891129494954595017379583319528532088055111254069874715852386305071569329096329522744304355766896648950445244523161731856403098711121722383113622298934233803081353362766142828064444866452387493035890729629049156044077239071381051585930796086670172427121883998797908792274921901699720888093776657273330010533678812202354218097512545405947522435258490771167055601360483958644670632441572215539753697817977846174064955149290862569321978468622482839722413756570560574902614079729686524145351004748216637048440319989000889524345065854122758866688116427171479924442928230863465674813919123162824586178664583591245665294765456828489128831426076900422421902267105562632111110937054421750694165896040807198403850962455444362981230987879927244284909188845801561660979191338754992005240636899125607176060588611646710940507754100225698315520005593572972571636269561882670428252483600823257530420752963450

explodeInteger :: Integer -> [Integer]
explodeInteger number = map (\x -> read [x]) (show number)

largestProduct :: [Integer] -> Integer -> [Integer] -> [Integer]
largestProduct [] prod numbs = numbs
largestProduct (x:xs) prod numbs = let a = (product (x:(take 12 xs))) in if (a>prod) then (largestProduct xs a (x:(take 12 xs))) else largestProduct xs prod numbs

euler8 = largestProduct (explodeInteger bigNumber) 0 []

-- Euler problem 14 - Longest Collatz sequence
collatz n seq | n == 1 = seq ++ [n] -- Very naive implementation, see collatzz for cache workings
              | n `mod` 2 == 0 = collatz (n `div` 2) (seq ++ [n])
              | otherwise = collatz (3*n+1) (seq ++ [n])

-- This works, with cache, but somehow is really really slow and insanely memory intensive
collatzz :: Integer -> [Integer] -> [Int] -> Int
collatzz n seq cache | n== 1 = length seq + 1
                     | length seq > 0 && n < (seq !! 0) = length seq + (cache !! ((fromIntegral n)-2))
                     | n `mod` 2 == 0 = collatzz (n `div` 2) (seq ++ [n]) cache
                     | otherwise = collatzz (3*n+1) (seq ++ [n]) cache

largestCollatz n xs | n < 1 = xs -- Very slow
                    | otherwise = let a = collatz n [] in if length a > length xs then largestCollatz (n-1) a else largestCollatz (n-1) xs


coll :: [Integer] -> [Int] -> [Int]
coll [] cache = cache
coll (x:xs) cache = coll xs (cache ++ [(collatzz x [] cache)]) -- Probably haskell cannot handle this level of recursion

findMax [] i j n = j
findMax (x:xs) i j n = if x > n then findMax xs (i+1) i x else findMax xs (i+1) j n

--euler14 = findMax $ coll [2..1000000] [] -- In python this takes 1.4 seconds. But on haskell it takes >30gb of memory (time unknown)

--collatzArr = [length (collatz x []) | x <- [2..1000000]]
--euler14 = largestCollatz 1000000 [] -- WIP
