module Lab3 where

import Data.List
import System.Random
import Test.QuickCheck
import Lecture3
import SetOrd

-- Lab 3 assignment Group 3
-- By: Jelle Sinnige, Robbin Siepman, Sven Keunen, Jeffrey Chong
-- *The time indications are an average from the whole team*

-- Excercise 1 (Time spend ~60 minutes)
-- A contradiction is when the propositions are always false.
-- So it is a contradiction when it is not satisfiable
contradiction :: Form -> Bool
contradiction f = not $ satisfiable f
testContradiction = contradiction $ Cnj [p, (Neg p)]
-- Testing: For (p && !p) expecting it is contradiction, because all valuations
-- lead to false evaluation

-- It is a tautology if it is satisfied by all valuations.
tautology :: Form -> Bool
tautology f = all (\v -> evl v f) (allVals f)
testTautology = tautology $ Dsj [p, (Neg p)]
testTautology2 = tautology $ Impl (Dsj [p, q]) (Dsj [p, q])
-- Testing: For all valuations of (p || !p) it is true.
-- And for (p V q) -> (p V q) it is also always true.

-- A logical entailment is true if and only if all evaluations that satisfy B,
-- also satisfy A. But this is not reflective.
entails :: Form -> Form -> Bool
entails fa fb = all ( \v -> evl v fb ) [ x | x <- (allVals fa), (evl x fa)]
testEntails = entails p (Dsj [p, q])
testEntails2 = not $ entails (Dsj [p, q]) p
-- Testing: p ⊢ (p V q) is a logical entailment
-- But (p V q) ⊢ p is not an logical entailment

-- Equivalence is essentially a reflective entailment. Thus if fa entails fb,
-- and at the same time fb entails fa then the formulas are equivalent
equiv :: Form -> Form -> Bool
equiv fa fb = (entails fa fb) && (entails fb fa)
testEquiv = equiv (Impl p q) $ Impl (Neg q) (Neg p)
testEquiv2 = not $ equiv p (Dsj [p, q])
-- Testing: (a -> b) <=> (!b -> !a) is equivalent
-- But p <=> (p V q) is not equivalent, it can be an entailment


-- Excercise 2 (Time spend ~70 minutes)
-- The first 3 test cases show the intricate false notations that are not
-- parsed correctly using parse.

-- Definitions of repeated formulas
f1 = [Neg q]
f2 = [Cnj[p,(Dsj[q,(Neg r)])]]
f3 = [Impl (Cnj[p, q]) (Cnj[q, p])]

testParse1 = parse "(-2)" == f1 -- Unecessary brackets around -2
testParse2 = parse "* 1 + 2 - 3" == f2 -- missing brackets that should indicate presendence
testParse3 = parse "*(1 2) ==> *(2 1)" == f3 -- missing outside brackets, so only *(1 2) is parsed

-- For each false test case above, an correct notation is defined below
-- that can be parsed correctly by the parser.
testParse1' = parse "-2" == f1
testParse2' = parse "*(1 + (2 -3))" == f2
testParse3' = parse "(*(1 2) ==> *(2 1))" == f3 -- now the whole formula is parsed

-- Some extra test cases that are not parsed correctly
testParse4 = parse "*(1 +(2 -3)" == f2 -- missing the last bracket
testParse5 = parse "==> 1 2" == [Impl p q] --Implicaiton wrong location

-- Some extra test cases that are parsed correctly
testParse6 = parse "-+(2 1)" == [Neg(Dsj[q, p])]
testParse7 = parse "*(+(2 -3) 1)" == [Cnj[Dsj[q, (Neg r)], p]]

-- The test cases are tested manually. It took us quite some time finding out
-- what to parser accepted and parsed correctly. We have made a selection from
-- all the manually tested cases and defined some of them above.
-- A description is added to the falsely parsed formulas explaining what is wrong.


-- Exercise 3 (Time spend ~70 min)
formToCnf :: Form -> Form
-- Remove brackets
formToCnf (Prop a) = Prop a

-- Remove double negation
formToCnf (Neg (Neg a)) = formToCnf a

-- Use Morgan's Law to get rid of negates outside of Cnj and Dsj
formToCnf (Neg (Cnj [a, b])) = formToCnf (Dsj [Neg a, Neg b])
formToCnf (Neg (Dsj [a, b])) = formToCnf (Cnj [Neg a, Neg b])

-- The Distributive law
formToCnf (Dsj [a, Cnj [b, c]]) = formToCnf (Cnj [Dsj [a, b], Dsj [a, c]])

-- Recursion to go over whole formula
formToCnf (Dsj [a, b]) = Dsj [formToCnf a, formToCnf b]
formToCnf (Cnj [a, b]) = Cnj [formToCnf a, formToCnf b]
formToCnf a = a

-- CNF conversion
cnf form = formToCnf (arrowfree form)

-- Testing CNF conversion
cnfTest = cnf ((Impl (Neg (Prop 2)) (Neg (Prop 1))))
cnfTest2 = cnf (Equiv (Prop 1) (Prop 2)) -- ((a&&b)||!a) && ((a&&b)||!b)


-- Excercise 4 - a few hours
-- Generate bare property
val :: Integer -> [Char]
val x | x `mod` 2 == 0 = show (x+1)
      | otherwise = "-" ++ (show (x+1))

-- Generate simplistic property
generateForm n form | (length form) > 50 = form
    | x < 0 = generateForm (abs n) form
    | x == 0 = generateForm (n-2) (form ++ " +(" ++ (val n) ++ " " ++ (val (n-1)) ++ ")")
    | x == 1 = generateForm (n-2) (form ++ " *(" ++ (val n) ++ " " ++ (val (n-1)) ++ ")")
    | x == 2 = generateForm (n-2) (form ++ " (" ++ (val n) ++ " ==> " ++ (val (n-1)) ++ ")")
    | x == 3 = generateForm (n-2) (form ++ " (" ++ (val n) ++ " <=> " ++ (val (n-1)) ++ ")")
    where x = n `mod` 4

-- Generate nested properties
formGen n form | (length form) > 100 = form -- Recursion is infinite for some reason...
    | x < 0 = formGen (abs n) form
    | x == 0 = formGen (n-1) (" *(" ++ (generateForm n "") ++ " " ++ (generateForm (n-1) "") ++ ")")
    | x == 1 = formGen (n-1) (" +(" ++ (generateForm n "") ++ " " ++ (generateForm (n-1) "") ++ ")")
    | x == 2 = formGen (n-1) (" +(" ++ (generateForm n "") ++ " " ++ (generateForm (n-1) "") ++ ")")
    | x == 3 = formGen (n-1) (" +(" ++ (formGen (n-1) "") ++ " " ++ (generateForm (n-1) "") ++ ")")
    | x == 4 = formGen (n-1) (" (" ++ (formGen (n-2) "") ++ " ==> " ++ (formGen (n-2) "") ++ ")")
    | x == 5 = formGen (n-1) (" (" ++ (formGen (n-3) "") ++ " <=> " ++ (formGen (n-3) "") ++ ")")
    where x = n `mod` 6

testCnf :: Integer -> Bool
testCnf x = let form = parse (formGen x "") !! 0 in equiv form (cnf form)

-- Lab 3 Question 5
sub :: Form -> Set Form
sub (Prop x) = Set [Prop x]
sub (Neg f) = unionSet (Set [Neg f]) (sub f)
sub f@(Cnj [f1,f2]) = unionSet ( unionSet (Set [f]) (sub f1)) (sub f2)
sub f@(Dsj [f1,f2]) = unionSet ( unionSet (Set [f]) (sub f1)) (sub f2)
sub f@(Impl f1 f2) = unionSet ( unionSet (Set [f]) (sub f1)) (sub f2)
sub f@(Equiv f1 f2) = unionSet ( unionSet (Set [f]) (sub f1)) (sub f2)

testPresent :: Integer -> Bool
testPresent x = let form = parse (formGen x []) !! 0 in length [f | f <- (explodeForm form []), not (inSet f (sub form))] == 0

-- Calculates length of a given Set Form
subLengthHelper :: Set Form -> Integer -> Integer
subLengthHelper (Set []) n = n
subLengthHelper (Set (x:xs)) n = subLengthHelper (Set xs) (n+1)
subLength :: Set Form -> Integer
subLength xs = subLengthHelper xs 0 


-- New generator that only generates very simplistic forms that sub can handle. Parse will make sure
-- that we do not have nesting. (Because we miss outermost wrapping). Also note that we do not negate anymore
-- because then our explode length will no longer match.
generateForm2 n form | (length form) > 50 = form
    | x < 0 = generateForm (abs n) form
    | x == 0 = generateForm (n-2) (form ++ " +(" ++ (show n) ++ " " ++ (show (1)) ++ ")")
    | x == 1 = generateForm (n-2) (form ++ " *(" ++ (show n) ++ " " ++ (show (n-1)) ++ ")")
    | x == 2 = generateForm (n-2) (form ++ " (" ++ (show n) ++ " ==> " ++ (show (n-1)) ++ ")")
    | x == 3 = generateForm (n-2) (form ++ " (" ++ (show n) ++ " <=> " ++ (show (n-1)) ++ ")")
    where x = n `mod` 4
-- We can improve it a little bit if we do allow nesting for Cnj/Dsj sub parameters (in this recursion). This
-- would limit the parameters for sub Cnj/Dsj to two.

manualSubTest1 = subLength (sub (parse "--1" !! 0)) == 3
manualSubTest2 = subLength (sub (parse "-1" !! 0)) == 2 
manualSubTest3 = subLength (sub (parse "---1" !! 0)) == 4

testLength :: Integer -> Bool
testLength x = let form = parse (generateForm2 (abs x) "") !! 0 in length (explodeForm form []) == fromIntegral ( subLength (sub form))


explodeForm :: Form -> [Form] -> [Form]
explodeForm (Prop x) forms = forms ++ [Prop x]
explodeForm (Neg x) forms = forms ++ [Neg x]
explodeForm (Cnj xs) forms = forms ++ [Cnj []] ++ (concat [explodeForm y [] | y <- xs])
explodeForm (Dsj xs) forms = forms ++ [Dsj []] ++ (concat [explodeForm y [] | y <- xs])
explodeForm (Impl x y) forms = forms ++ [(Impl x y)] ++ (explodeForm x []) ++ (explodeForm y [])
explodeForm (Equiv x y) forms = forms ++ [(Equiv x y)] ++ (explodeForm x []) ++ (explodeForm y [])



-- Lab 3 Question 6
-- Spend approx 1 hour
-- CNF notation allows us to not expect nested Dsj and Conj

-- We only Handle Prop and Negate here. These are the only legal arguments.
formToInt :: Form -> Int
formToInt (Prop a) = read (show a)
formToInt (Neg (Prop a)) = -(formToInt (Prop a))

-- We only handle Dsj, Neg, and Prop here. These are the only legal arguments
formToIntArr :: Form -> [Int]
formToIntArr (Prop a) = [formToInt (Prop a)]
formToIntArr (Neg (Prop a)) = [-formToInt (Prop a)]
formToIntArr (Dsj xs) = [formToInt x | x <- xs]
formToIntArr _ = [0] -- Error case

-- Given input is a Conjunction or any in [Dsj, Neg, Prop], which is handled by formToIntArr
cnf2cls :: Form -> [[Int]]
cnf2cls (Cnj xs) = [formToIntArr x | x <- xs]  
cnf2cls a = [formToIntArr a]

parseHelper :: String -> Form
parseHelper s = parse s !! 0

testClsHelper :: String -> [[Int]] -> Bool
testClsHelper formula expectation = cnf2cls (parseHelper formula) == expectation 
testCls1 = testClsHelper "5" [[5]]
testCls2 = testClsHelper "-99" [[-99]]
testCls3 = testClsHelper "*(4 +(5 6))" [[4], [5, 6]]
testCls4 = testClsHelper "*(4 +(5 -6))" [[4], [5, -6]]



main :: IO ()
main = do
  putStrLn "===Excercise 1==="
  print testContradiction
  print testTautology
  print testTautology2
  print testEntails
  print testEntails2
  print testEquiv
  print testEquiv2
  putStrLn "\n===Excercise 2==="
  print testParse1
  print testParse2
  print testParse3
  print testParse1'
  print testParse2'
  print testParse3'
  putStrLn "extra test cases:"
  print testParse4
  print testParse5
  print testParse6
  print testParse7
  putStrLn "\n===Excercise 3==="
  print cnfTest
  print cnfTest2
  putStrLn "\n===Excercise 4==="
  quickCheck testCnf
  putStrLn "\n===Excercise 5==="
  quickCheck testPresent
  print manualSubTest1
  print manualSubTest2
  print manualSubTest3
  putStrLn "\n===Excercise 6==="
  print testCls1
  print testCls2
  print testCls3
  print testCls4
