module Helper where

import SetOrd
import System.Random
import Data.List (sort,nub)
-- Sourced from earlier assignments

-- since 1
primes :: [Integer]
primes = 2 : filter prime [3..]

prime :: Integer -> Bool
prime n = n > 1 && all (\ x -> rem n x /= 0) xs
  where xs = takeWhile (\ y -> y^2 <= n) primes

nextPrime :: Integer -> Integer
nextPrime n = if prime n then n else nextPrime (n+1)

seed = randomIO :: IO Int -- Wrap IO here so we can keep our generators pure

infiniteRandomInts :: Int -> [Int]
infiniteRandomInts seed = randoms (mkStdGen seed) :: [Int] -- Maybe it makes more sense to limit the range e.g. -100 to 100

randomInts :: Int -> Int -> [Int]
randomInts seed max = let xs = infiniteRandomInts seed in take (head xs `mod` max) (tail xs)

randomInt :: Int -> Int
randomInt seed = head (infiniteRandomInts seed)

randInt :: Int -> Int -> Int -> Int
randInt _ 0 0 = 0
randInt seed min 0 = (-1) * (randInt seed 0 ((-1)*min))
randInt seed min max = let number = randomInt seed in if number < min then (min + (abs number)) `mod` max else number `mod` max

-- since 2
--insert elements if not present
insertUnique :: Ord a => a -> Set a -> Set a
insertUnique x y
    | inSet x y = y
    | otherwise = insertSet x y

-- zip merge 2 lists, very basic implementation
merge :: [a] -> [a] -> [a]
merge x [] = x
merge [] x = x
merge (x:xs) (y:ys) = x:y: (merge xs ys)

-- remove from set
removeFromSet :: Ord a => a -> Set a -> Set a
removeFromSet _ (Set []) = Set []
removeFromSet a (Set (x:ys))
    | a == x = (Set ys)
    | otherwise = insertSet x (removeFromSet a (Set ys))

-- 3
-- inRelation :: Ord a => (a,a) -> Rel a -> Bool
-- inRelation _ (Rel []) = False
-- inRelation x (Rel (c: d))
--     |  (x == c) = True
--     | otherwise = inRelation x (Rel d)

-- insertRelation :: Ord a => (a,a) -> Rel a -> Rel a
-- insertRelation a (Rel c) = (Rel (a:c))

-- removeRelation :: Ord a => (a,a) -> Rel a -> Rel a
-- removeRelation a (Rel []) = (Rel [])
-- removeRelation a (Rel (x:ys))
--     | a == x = (Rel ys)
--     | otherwise = insertRelation x (removeRelation a (Rel ys))

-- mirrorRelation :: Ord a => (a,a) -> (a,a)
-- mirrorRelation (a,b) = (b,a)

removeFromList :: Eq a => a -> [a] -> [a]
removeFromList _ [] = []
removeFromList a (x:ys)
  | a == x = removeFromList a ys
  | otherwise = x:(removeFromList a ys)