import LTS
import Data.List
import System.Random
import Test.QuickCheck

-- ========= LAB 6 =========

-- ASS 1
-- there were 2 methods of reasoning for this excersise: 
-- 1 - the paper defines very little of what makes an LTS valid 
-- thus we apply those little rules
-- 2 - we apply the rules we expect: all states are reachable

-- these are functions to test LTS validity
stateLessLTS :: LTS
stateLessLTS = ([],["?but", "tea"],[],1);

transitionLessLTS :: LTS
transitionLessLTS = ([1,2],[],[(1,"?but",2)],1);

invalidStartLTS :: LTS
invalidStartLTS = ([1,2,3],["?but","!liq"],[(1,"?but",2),(2,"!liq",3)],6);

-- Generators
generateLtsModel numberOfStates = let n = (abs numberOfStates) `mod` 30 + 1 in [(x, show x ++ "_to_" ++ show y, y) | x <- [1..n], y <- [1..n]]
generateBrokenLtsModel numberOfStates = let n = (abs numberOfStates) `mod` 30 + 1 in [(x, "delta", y) | x <- [1..n], y <- [1..(n-1)]]


-- this was made, following reasoning 1
validateLTS :: LTS -> Bool
validateLTS ([],[],c,d) = False -- no transitions or states = immedeatly fail
validateLTS (a,b,c@(hc@(ca,cb,cc):[]),d) = (d `elem` a) && (ca `elem` a) && (cc `elem` a) && (cb `elem` b) -- there is a match
validateLTS (a,b,c@(hc@(ca,cb,cc):cs),d) = (d `elem` a) && (ca `elem` a) && (cc `elem` a) && (cb `elem` b) && validateLTS (a,b,cs,d)
validateLTS (_,_,_,_) = False

-- -- this, following reasoning 2 - it unfortunately has an compile error
-- validateLTS :: LTS -> Bool
-- validateLTS lts = (validateStateTransitions lts) && (validateLabelTransitions lts) && (allStatesReachable lts)

-- statesInTransitions (states, _, transitions, _) = sort. nub $ concat [[s0, s1] | (s0, _, s1) <- transitions]
-- labelsInTransitions (_, _, transitions, _) = sort . nub $ [l | (_, l, _) <- transitions]

-- validateStateTransitions lts@(states,_,_,startState) = ((sort states) == (statesInTransitions lts)) && (startState `elem` states)
-- validateLabelTransitions lts@(_,labels,_,_) = ((sort labels) == (labelsInTransitions lts))

-- ltsPathState transitions state = [transition | transition@(s,_,_) <- transitions, s == state]
-- ltsPaths (states, _, transitions,_) = [ltsPathState transitions x | x <- (sort states)]

-- -- Convert transition to Transitive Closure, so we can use helper functions from previous weeks
-- convertTransitions (_,_,transitions,_) = [(s1,s2) | (s1,_,s2) <- transitions] 

-- allStatesReachable :: LTS -> Bool
-- allStatesReachable lts@(states, _, transitions, startState) = let trans = trClos (convertTransitions lts) in all (==True) [(startState, state) `elem` trans | state <- states, state /= startState]

-- QuickCheck Test Cases
testValidModelGeneration n = let lts = createLTS (generateLtsModel n) in validateLTS lts
testValidModelGeneration2 n = let lts = createLTS (generateBrokenLtsModel n) in not (validateLTS lts)


-- ASS 2
sGen :: Gen State
sGen = do
    x <- choose (1,6)
    return x

lGen :: Gen Label
lGen = elements [ "!a", "!b", "!c", "?a", "?b", "?c" ]

tsGen :: Gen LabeledTransition
tsGen = do
    a <- sGen
    b <- lGen
    c <- sGen
    return (a,b,c)

ltsGen :: Gen LTS
ltsGen = do
    x <- listOf tsGen
    return (createLTS x)

-- ASS 3

-- generator classes are strange, we have these that generate LTS's from a number
generateRandomLts seed = let numbs = take 10 (randomIntsMax seed 10) in createLTS [(x, show x ++ "_to_" ++ show y, y) | x <- (take 5 numbs), y <- (drop 5 numbs)]
randomInts seed = randoms (mkStdGen seed) :: [Integer]
randomIntsMax seed max = [abs n `mod` max | n <- (randomInts seed)]


without :: Eq t => [t] -> t -> [t]
without xs y = [x | x <- xs, x /= y]

-- Same as without, but for traces specifically
withoutTrace xs ys = [x | x <- xs, not (x `elem` ys)]

-- Prepends xs for all items in ys
-- E.g. prepend [1] [[2,3],[4,5]] -- [[1,2,3],[1,4,5]]
prepend :: [a] -> [[a]] -> [[a]]
prepend xs ys = [xs ++ y | y <- ys]

-- prepend trace
increaseTrace :: [a] -> [a] -> [[a]]
increaseTrace xs ys = [xs ++ [y] | y <- ys]

traces :: LTS -> [[LabeledTransition]]
traces (_,_,transitions,startState) =
    let start = possibleStateTransitions transitions startState
        startTraces = [[trace] | trace <- start]
        traces = findTraces transitions startTraces
    in [trace | trace <- traces, trace /= []] 

bareTraces :: [[LabeledTransition]] -> [[Label]]
bareTraces [] = [[]]
bareTraces (trace:traces) = sort ([[label | (_,label,_) <- trace]] ++ bareTraces traces `without` [])

straces :: LTS -> [Trace]
straces lts = bareTraces (traces lts)
-- Keep in mind that this returns a finite list of traces because there is no (infinite) looping
-- To generate infinite patterns, the final state of a trace must be matched with the starting state
-- of a trace. This is implemented at question 6, called combineTraces, for the SmartDoor use case.

findTracesHelper transitions trace =
    let remainder = transitions `withoutTrace` trace
        current = last trace
        nextTransitions = (possibleTransitions remainder current)
    in if nextTransitions == [] then [trace] else findTraces transitions (increaseTrace trace nextTransitions)

findTraces transitions [] = [[]]
findTraces transitions (trace:traces) = findTracesHelper transitions trace ++ findTraces transitions traces 

possibleStateTransitions :: [LabeledTransition] -> State -> [LabeledTransition]
possibleStateTransitions available state = [t | t@(tState,_,_) <- available, tState == state]

possibleTransitions :: [LabeledTransition] -> LabeledTransition -> [LabeledTransition]
possibleTransitions available (_,_,state) = possibleStateTransitions available state 

possibleLabelTransitions :: [LabeledTransition] -> Label -> [LabeledTransition]
possibleLabelTransitions available label = [t | t@(_,tLabel,_) <- available, (tLabel == label)]

-- Create a lot of traces
tracesGenerator n = straces (createLTS (generateLtsModel n))
-- Generates random traces
generateRandomTraces seed = straces $ generateRandomLts seed

-- ASS 4
after :: LTS -> [Label] -> [State]
after lts@(_,_,transitions,startState) labels = afterHelper lts (possibleStateTransitions transitions startState) labels 

afterHelper _ current [] = [s | (_,_,s) <- current]
afterHelper lts@(_,_,transitions,_) current (label:labels) =
    let routes = possibleLabelTransitions current label
        next = concat [possibleTransitions transitions route | route <- routes]
    in if next == [] && labels == [] then afterHelper lts routes [] else afterHelper lts next labels  

-- ASS 5
-- Since the traces function, and thus straces as well, find all unique possible path patterns we
-- can rely on that to do the heavy lifting. The after function can test the path patterns of each
-- LTS. In the IOCO implementation we can just make sure that all the traces in the leading model
-- are valid traces in the checked model as well.
-- lts1 is MODEL.
isIOCO :: LTS -> LTS -> Bool
isIOCO lts1 lts2  = let ts = straces lts1 in all (==True) [length (after lts2 t) == 1 | t <- ts]

-- ASS 6
--Using this correct model
--doorImpl1 :: State -> Label -> (State, Label)
--doorImpl1 0 "?close" = (1, "!closed")
--doorImpl1 1 "?open" = (0, "!opened")
--doorImpl1 1 "?lock" = (2, "!locked")
--doorImpl1 2 "?unlock" = (1, "!unlocked")
--doorImpl1 _ _ = error "Invalid command and/or state!"
--The LTS is:
doorLTS = createLTS [(0, "?close", 1), (1, "!closed", 2), (2, "?open", 3), (3, "!opened", 0), (2, "?lock", 4), (4, "!locked", 5), (5, "?unlock", 6), (6, "!unlocked", 2)]

testTrace :: [Label] -> State -> (State -> Label -> (State, Label)) -> Bool 
testTrace [] _ _ = True
testTrace (action:reaction:rest) state impl =
    let (newState, newReaction) = impl state action
    in if newReaction == reaction then testTrace rest newState impl else False


-- Here we take a certain depth and create subsequences of traces. We can chain traces here because our model is created
-- in such a way that it always returns to the starting state.
combineTraces traces depth =
    let places = concat [[y | y <- [0..(length traces - 1)]] | x <- [0..depth]]
        placeAttempts = [x | x <- subsequences places, x /= []]
    in [concat [traces !! x | x <- xs] | xs <- placeAttempts]

-- Checks many different kind of traces (depth=5) against the SUT
testLTSAgainstSUT lts sut = all (==True) [testTrace trace 0 sut | trace <- combineTraces (straces lts) 5]

-- ASS 7

main :: IO ()
main = do
    putStrLn "===Exercise 1==="
    -- these 6 should return false
    print (validateLTS stateLessLTS) -- return false
    -- print (validateLTS' stateLessLTS) -- return false
    print (validateLTS transitionLessLTS) -- return false
    -- print (validateLTS' transitionLessLTS) -- return false
    print (validateLTS invalidStartLTS) -- return false
    -- print (validateLTS' invalidStartLTS) -- return false
    -- these should pass
    quickCheck testValidModelGeneration -- return true
    quickCheck testValidModelGeneration2 -- return 2
    putStrLn "===Exercise 3==="
    print (straces counterModel == straces counterImpl)
    print (straces coffeeModel1 /= straces coffeeImpl1) -- Model has tea, but impl does not
    print (straces coffeeModel2 /= straces coffeeImpl2) -- Impl has tea, but model does not
    print (straces coffeeModel3 /= straces coffeeImpl3) -- Impl has button and tea, but model does not
    print (straces coffeeModel4 /= straces coffeeImpl4) -- Model has button for tea, impl does not
    print (straces coffeeModel5 /= straces coffeeImpl5) -- Implementation can deadlock after coin, model cannot 
    print (straces coffeeModel6 /= straces coffeeImpl6) -- These are actually equivalent, but model has `tau` bug. Needs fixing
    putStrLn "===Exercise 5==="
    print (isIOCO counterModel counterImpl)
    print (not (isIOCO coffeeModel1 coffeeImpl1)) -- Implementation has no tea
    print (isIOCO coffeeModel2 coffeeImpl2) -- Implementation has extra features
    print (isIOCO coffeeModel3 coffeeImpl3) -- Implementation has extra features
    print (not (isIOCO coffeeModel4 coffeeImpl4)) -- Implementation has no button
    print (isIOCO coffeeModel5 coffeeImpl5) -- Just hope you never deadlock
    print (not (isIOCO coffeeModel6 coffeeImpl6)) -- No tau state in Implementation. Probably due to the known bug.
    putStrLn "==Exercise 6==="
    print (testLTSAgainstSUT doorLTS doorImpl1)
    print (not $ testLTSAgainstSUT doorLTS doorImpl2)
    print (not $ testLTSAgainstSUT doorLTS doorImpl5)