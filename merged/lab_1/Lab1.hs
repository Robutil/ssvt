--module Lab1 where

import Data.List
import Test.QuickCheck
import Lab1Helper

-- Question 1: Redo exercises 2 and 3 of Workshop 1 by writing QuickCheck tests for these statements.
-- Question 1 - Part 1

-- This function creates an array of length n starting at 1 where all natural numbers are squared.
sumSquares :: Integer -> Integer
sumSquares n = sum [x^2 | x <- [1..n]]

-- This is an alternative approach, however it requires the reader to mental map the $ operator
-- brackets (and the same for the map function, although the same can be said for the list
-- comprehension)
-- sumSquares n = sum $ map (^2) [1..n]

-- This is what we proved with induction. Now it serves as our test case.
sumSquaresProperty :: Integer-> Integer
sumSquaresProperty n = ((n * (n + 1) * (2 * n + 1)) `div` 6)

-- QuickCheck on the sum of squares formulas
testSumSquares :: Integer -> Bool
testSumSquares n = let a = abs n in sumSquares a == sumSquaresProperty a


-- Question 1 - Part 2
-- This function creates an array of length n starting at 1 where all natural numbers are kubed.
sumKubes :: Integer -> Integer
sumKubes n = sum [x^3 | x <- [1..n]]

-- This is what we proved with induction. Now it serves as our test case.
sumKubesProperty :: Integer -> Integer
sumKubesProperty n = ((n * (n+1)) `div` 2) ^ 2

-- QuickCheck on the sum of kubes formulas
testSumKubes :: Integer -> Bool
testSumKubes n = let a = abs n in sumKubes a == sumKubesProperty a


-- Question 2
-- Sum sets in powerset
sumPowerset :: Integer -> Int
sumPowerset n = length (subsequences [1..n])
-- In case we want the function signiture Integer -> Integer, use fromIntegral

-- Sum sets with power of two (Integer to Int can overflow, as noted you can use fromInt)
sumPowersetProperty :: Integer -> Int
sumPowersetProperty n = 2^n

-- QuickCheck between two statements for powerset length
testPowerset :: Integer -> Bool
testPowerset n = (n >= 0 && n < 25) --> (sumPowerset n ==  sumPowersetProperty n)

-- The property is hard to test, because the length of sets changes with
-- a power of n. Meaning that if n = 20 there are more than a milion sets that
-- the function sebsequences must generate. This takes a lot of time and it is
-- therefore hard to test if we want to test an extensive amount of cases.
--
-- The test is testing if the amount of sets that subsequences generates equals
-- to the formula 2^n. So we are partly checking for the mathematical fact that
-- the two statements give the same result and also if subsequences does what
-- it claims to do (as well as the length function).


-- Question 3
permutationLength :: Integer -> Integer
permutationLength n = fromIntegral $ length $ permutations [1..n]

permutationLengthProperty :: Integer -> Integer
permutationLengthProperty n = product [1..n]

testPermutationLength :: Integer -> Bool
testPermutationLength n = (n>0 && n<10) --> (permutationLength n == permutationLengthProperty n)

-- Just like the powerset testing in exercise 2, this property is also hard to
-- test in a way. The reason is that the amount of permutation is equal to the
-- factorial of the amount of elements in the list. Which means that if there
-- is a list consisting of 6 elements there are 720 permutations/combinations.
--
-- Again we are testing two things. First whether the factorial of n-elements
-- is equal to the length of the permutations of a list with n elements.
-- Consequently we are also testing if the permutations function does what we
-- would mathematically expect.


-- Question 4
findReversePrimes :: Integer -> [Integer]
findReversePrimes n = [x | x <- [1..n], prime x, prime(reversal x)] -- x <- (somePrimes n)
-- Optimalizations include: lookup isReversePrime in shared prime list
-- Test by checking individual primes and their reverse prime for validity, and checking
-- a few length of sets

findReversePrimesBelowTenThousand = findReversePrimes 10000
-- It's quite hard to test this; because it's a constant value. You can check that the numbers
-- found are primes, I suppose.
testReversePrimes = premiseAllPrime findReversePrimesBelowTenThousand


-- Question 5
consecutivePrimes :: [Integer] -> [Integer]
consecutivePrimes (x:xs) = if (prime (sum (x:(take 100 xs)))) then x:(take 100 xs) else consecutivePrimes xs

premiseAllPrime :: [Integer] -> Bool
premiseAllPrime xs = all prime xs

consecutivePrimesResult = consecutivePrimes primes

-- These are the tests, although they are not "QuickTests"
mustAllBePrime = premiseAllPrime consecutivePrimesResult
otherConSecChecks = length consecutivePrimesResult == 101 && prime ( sum consecutivePrimesResult )
-- Because we iteratively take larger primes, the first 101 that form a prime number
-- is the smallest. We can show that it is capable of finding the smallest if we show
-- that it can find a bigger one. This can be easily done with removing the first from
-- the found 101 and adding the infinite prime list after that (little bit more tricky
-- to find the indexes).


-- Question 6
-- refute (p1 * p2 * ... * pn) + 1 == prime
-- Take all possible ordered sets that have a different length but smaller than `take x primes`
-- Check, in order by size, if premise is invalid. If so return, else continue.
-- If subsets are exhausted increase x by one, repeat (preferable cache previous iteration results)
refutePrimes :: Int -> [Integer] -> [Integer]
refutePrimes n xs = if (refutePrimesPremise (take n xs)) then (take n xs) else refutePrimes (n+1) xs
refutePrimesPremise xs = not (prime ((product xs) + 1))
-- The lowest number found is 6. This is the  list [2,3,5,7,11,13],
-- the product of this list is 30031, so 30031 which is divisable by 59 and 509 so this refutes the statement
-- The test case in this case that the refutedPrime list are all primes, and that their product is not a prime. Other test cases would be that for smaller lists the products are in fact prime.

-- Question 7
splitIntoDigs :: Integer -> [Integer]
splitIntoDigs 0 = []
splitIntoDigs n = splitIntoDigs (n `div` 10) ++ [n `mod` 10]

luhnDouble :: Integer -> Integer
luhnDouble n = if n*2 > 9
  then (2*n)-9
    else 2 * n

luhnDoubleAlternating :: Integer -> [Integer] -> [Integer]
luhnDoubleAlternating n [] = []
luhnDoubleAlternating n (x:xs) = if n == 0
  then x : luhnDoubleAlternating 1 xs
else luhnDouble x : luhnDoubleAlternating 0 xs

getCheckSumDigit :: Integer -> Integer
getCheckSumDigit n = head (splitIntoDigs (reversal n))

getLuhnDigits :: Integer -> [Integer]
getLuhnDigits n = luhnDoubleAlternating 1 (tail (splitIntoDigs (reversal n)))

luhn :: Integer -> Bool
luhn n =  getCheckSumDigit n == sum(getLuhnDigits n) * 9 `mod` 10
--Source: https://stackoverflow.com/a/1918522
joiner :: [Integer] -> Integer
joiner = read . concatMap show

isAmericanExpress :: Integer -> Bool
isAmericanExpress n = length(splitIntoDigs n)== 15 && luhn n

isMaster :: Integer -> Bool
isMaster n = length(splitIntoDigs n)== 16 && luhn n && ((joiner (take 2 (splitIntoDigs n)) >= 51 &&  joiner(take 2 (splitIntoDigs n)) <= 55) || (joiner(take 4 (splitIntoDigs n)) >= 2221 &&  joiner(take 4 (splitIntoDigs n)) <= 2720))

isVisa  :: Integer -> Bool
isVisa  n = length(splitIntoDigs n)== 16 && luhn n && joiner (take 1 (splitIntoDigs n)) == 4

-- Tested with the wikis example 7992739871, without its check digit 3 it is false, with it we return true
-- For every other check digit we return false
-- I think a good way to test these functions would be to get a couple dozen of numbers which are correct
-- Then randomly substitute the CheckSumDigits and verify that this returns false
-- For visa/master/AE :
-- Some examples can be found here https://www.freeformatter.com/credit-card-number-generator-validator.html


-- Question 8
data Boy = Matthew | Peter | Jack | Arnold | Carl
           deriving (Eq,Show)
boys = [Matthew, Peter, Jack, Arnold, Carl]

accuses :: Boy -> Boy -> Bool
accuses Matthew Carl = False -- This is not necessary because of the base case, but it makes sense reading the assigment
accuses Matthew Matthew = False -- Same as above comment
accuses Matthew _ = True
accuses Peter Matthew = True
accuses Peter Jack = True
accuses Jack someBoy = not (accuses Matthew someBoy) && not (accuses Peter someBoy)
accuses Arnold someBoy = (accuses Matthew someBoy) /= (accuses Peter someBoy)
accuses Carl someBoy = not (accuses Arnold someBoy)
accuses _ _ = False

accusers :: Boy -> [Boy]
accusers someBoy = [x | x <- boys, accuses x someBoy]

guilty, honest :: [Boy]
guilty = [x | x <- boys, length(accusers x) == 3] -- Three boys are speaking the truth
honest = accusers (head guilty)

testCsi :: Bool
testCsi = length guilty == 1 && length honest == 3


-- The test cases
main = do
    quickCheck testSumSquares
    quickCheck testSumKubes
    quickCheck testPowerset
    quickCheck testPermutationLength
    print testReversePrimes
    quickCheck testReversePrimes
    print mustAllBePrime
    quickCheck testReversePrimes
    print otherConSecChecks
    quickCheck otherConSecChecks
    print (refutePrimes 1 primes) -- Product 30031 and n=6
    print (isVisa 4417123456789113)
    quickCheck (isVisa 4417123456789113)
    print guilty -- Guilty boy
    print honest -- Honest boy
    quickCheck testCsi
