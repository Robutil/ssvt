
module Lab2 where

import Data.List
import Data.Char
import Data.Maybe
import System.Random
import Test.QuickCheck
import Lecture2

infix 1 -->

(-->) :: Bool -> Bool -> Bool
p --> q = (not p) || q

-- exercise 1; randomness checking
-- Checking if funciton probs generates randomness as expected
probs :: Int -> IO [Float]
probs 0 = return []
probs n = do
    p <- getStdRandom random
    ps <- probs (n-1)
    return (p:ps)

-- from list extract values that are inbetween n and m
quartiles :: Float -> Float -> [Float] -> [Float]
quartiles n m [] = []
quartiles n m (x:xs)
    | x >= n && x < m = [x] ++ quartiles n m xs
    | otherwise = quartiles n m xs

-- check if the amount of values found between n and m are approximately
-- 25% of the total list
proportionCheck :: Float -> Float -> [Float] -> Bool
proportionCheck n m xs = length (quartiles n m xs) >= 2350 &&
    length (quartiles n m xs) <= 2650

-- execute the proportionCheck for each quartile
proportionExpected :: Float -> Float -> [Float] -> Bool
proportionExpected n m xs
    | m < 1.0 && proportionCheck n m xs =
            proportionExpected ((+) n 0.25) ((+) m 0.25) (xs \\ quartiles n m xs)
    |m == 1.0 && proportionCheck n m xs = True
    |otherwise = False

-- test if probs is really generating random floats
testProbs :: IO Bool
testProbs = do
    list <- probs 10000
    let result = proportionExpected 0.0 0.25 list
    return result

-- With the margin chosen from 23.5% to 26.5%, the randomness of the numbers
-- in the created list is correct. The amount of numbers in each quartile is
-- randomly distributed

-- exercise 2; recognizing triangles
data Shape = NoTriangle | Equilateral
           | Isosceles  | Rectangular | Other deriving (Eq,Show)

triangle :: Integer -> Integer -> Integer -> Shape
triangle x y z
    | not(x + y > z && x + z > y && z + y > x) = NoTriangle
    | x == y && y == z = Equilateral
    | x == y || x == z || y == z = Isosceles
    | x^2 + y^2 == z^2 || x^2 + z^2 == y^2 || z^2 + y^2 == x^2 = Rectangular
    | otherwise = Other

-- the function is checked by manually inserting triples that correspond to
-- one of the triangle types and compare it with the output of the function

-- exercise 3; Testing properties strength
-- a). the properties from workshop 2 exercise 3
p1, p2, p3, p4 :: Integer -> Bool
p1 = \x -> even x
p2 = \x -> even x && x > 3
p3 = \x -> even x || x > 3
p4 = \x -> even x && x > 3 || even x

-- b). Descending strength list of all the implemented properties
-- domain = [(-10)..10]
-- quicksortStrength :: [(a -> Bool)] -> [(a -> Bool)]
-- quicksortStrength [] = []
-- quicksortStrength (x:xs) =
--    quicksortStrength [ a | a <- xs, stronger domain x a]
--    ++ [x]
--    ++ quicksortStrength [ a | a <- xs, weaker domain x a]
--
-- -- sortStrength :: [a -> Bool]
-- sortStrength = quicksortStrength [p1, p2, p3, p4]

-- [p2,p1,p4,p3] with p1 and p4 equally strong

-- exercise 4; Recognizing permutations
-- check if both lists consist of same elements
findValue :: Eq a => [a] -> [a] -> Bool
findValue [] ps = True
findValue xs ps
    | (head xs) `elem` ps = findValue (tail xs) ps
    | otherwise = False

-- check if lists are same length
isPermutation :: Eq a => [a] -> [a] -> Bool
isPermutation xs ps
    | length(xs) == length(ps) = findValue xs ps
    | otherwise = False

-- Test different properties
testList = [-5..5]
testList1 = [5..15]
testList2 = testList ++ testList1
testList3 = testList1 ++ testList
testList4 = permutations [5..10] !! 30
testList4' = permutations [5..10] !! 710

testPerm = isPermutation testList testList -- True
testPerm1 = isPermutation testList testList1 -- False
testPerm2 = isPermutation testList testList2 -- False
testPerm3 = isPermutation testList2 testList3 -- True
testPerm4 = isPermutation testList4 testList4 -- True
testPerm4' = isPermutation testList4 testList4' -- True
testPerm5 =  isPermutation testList2 testList4' -- False

-- Odered list by strength of properties
testListStrength = \x -> length[-x..x] == x - (-x)
testListStrength1 = \x -> length[x..(x+10)] == (x+10) + 1

-- testStrength :: [String]
-- testStrength
--     | stronger domain testListStrength testListStrength1 =
--         ["testListStrength"] ++ ["testListStrength1"]
--     | otherwise = ["testListStrength1"] ++ ["testListStrength"]

-- Automation for testing

-- exercise 5 (This is part of Robbin's implementation)
isDerangement :: Eq b => [b] -> [b] -> Bool
isDerangement xs ys = isPermutation xs ys && (length [x | x <- zip xs ys, fst x == snd x]) == 0

deran :: (Eq t, Num t, Enum t) => t -> [[t]]
deran n = let a = [0..n] in [x | x <- permutations a, isDerangement x a]

-- exercise 6
-- ROT13 is a substitution cipher that changes a letter with the 13th letter after it,
-- in the alphabet. This makes it possible to encoding and decoding using the
-- the same algorithm.

-- initiate list with letters so each letter from 'a' untill 'Z' can move
-- 13 elements up
lowerAlphabet = ['a'..'z'] ++ ['a'..'m']
upperAlphabet = ['A'..'Z'] ++ ['A'..'M']

-- get letter that is 13 elements after input letter
rot13 :: Char -> Char
rot13 c
    | (findIndex (==c) lowerAlphabet) /= Nothing =
        lowerAlphabet !! ((fromMaybe 0 (findIndex (==c) lowerAlphabet)) + 13)
    | (findIndex (==c) upperAlphabet) /= Nothing =
        upperAlphabet !! ((fromMaybe 0 (findIndex (==c) upperAlphabet)) + 13)
    | otherwise = c

-- basic test for rot13 implementation
rot13Check :: Char -> Bool
rot13Check c = rot13(rot13 c) == c

-- main :: IO()
main = do
    testProbs
    print (triangle 15 15 10)
