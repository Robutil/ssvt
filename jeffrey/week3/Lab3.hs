module Lab3 where

import Data.List
import System.Random
import Test.QuickCheck
import Lecture3
import SetOrd
import Control.Monad

-- exercise 1 - 90 min
satisfiable :: Form -> Bool
satisfiable f = any (\v -> evl v f) (allVals f)

-- there is non that is true, so opposite of satisfiable
contradiction :: Form -> Bool
contradiction f = not (satisfiable f)
testContradiction = contradiction (Cnj[p,(Neg p)]) -- Basic true example

-- tautology is true if whole truth table is satisfied
tautology :: Form -> Bool
tautology f = all (\v -> evl v f) (allVals f)
testTautology = tautology (Dsj[(Impl p q), (Impl q p)]) -- basic true example

-- logical entailment
-- is True if all evaluations that satisfy B, also satisfy A
entails :: Form -> Form -> Bool
entails f f' = all (\v -> evl v f) [v | v <- allVals f', evl v f']
testEntails = entails (Cnj[p, q]) (Cnj[q, p]) -- basic true example

-- logical equivalence
-- same as entailment including the other way around
equiv :: Form -> Form -> Bool
equiv f f' = entails f f' && entails f' f
testEquiv = equiv (Cnj[p, q]) (Cnj[q, p]) -- basic true example

-- exercise 2 - 60 min
-- Testing parse function
testParse = parse "1" == [p] -- True
testParse2 = parse "+(2-3)" == [Dsj[q, (Neg r)]] -- True
testParse3 = parse "(*(1 2) ==> *(2 1))" == [Impl (Cnj[p, q]) (Cnj[q, p])] -- True
testParse4 = parse "*(1+(2-3))" == [Cnj[p,(Dsj[q,(Neg r)])]] --True
testParse5 = parse "1 ==> 2" == [Impl p q] -- False missing brackets, only 1 is parsed
testParse6 = parse "(-3)" /= [Neg r] -- False, unecessary brackets
testParse7 = parse "*(1 +(2 -3)" /= [Cnj[p,(Dsj[q,(Neg r)])]] -- False, missing last bracket


-- exercise 3 - Got really stuck in this assignment.
-- This is Robbin's implementation, that is also in the merge file.
formToCnf :: Form -> Form
formToCnf (Prop a) = Prop a -- Remove brackets

-- Remove double negation
formToCnf (Neg (Neg a)) = formToCnf a

-- Use Morgan's Law to get rid of negates outside of Cnj and Dsj
formToCnf (Neg (Cnj [a, b])) = formToCnf (Dsj [Neg a, Neg b])
formToCnf (Neg (Dsj [a, b])) = formToCnf (Cnj [Neg a, Neg b])

-- The Distributive law
formToCnf (Dsj [a, Cnj [b, c]]) = formToCnf (Cnj [Dsj [a, b], Dsj [a, c]])

-- Recursion to go over whole formula
formToCnf (Dsj [a, b]) = Dsj [formToCnf a, formToCnf b]
formToCnf (Cnj [a, b]) = Cnj [formToCnf a, formToCnf b]
formToCnf a = a

-- CNF conversion
cnf form = formToCnf (arrowfree form)

-- Testing CNF conversion
cnfTest = cnf ((Impl (Neg (Prop 2)) (Neg (Prop 1))))
cnfTest2 = cnf (Equiv (Prop 1) (Prop 2))

-- Excercise 4 -
-- From last weeks Lecture2.hs
-- getRandomInt :: Int -> IO Int
-- getRandomInt n = getStdRandom (randomR (0,n))
--
-- genForm :: Int -> IO Form
-- genForm d = if d == 0 then
--         return (Prop (getRandomInt 10))
--     else if d == 1 then
--         return (Neg (d + 1)
--     else if d == 2 then
--         return (Cnj (generateForm (d-1) (getRandomInt 10)))
--     else
--         return (Dsj (generateForm (d-1) (getRandomInt 10)))
--
-- generateForm :: Int -> Int -> IO [Form]
-- generateForm _ 0 = return []
-- generateForm d n = do
--             fa <- genForm d
--             fb <- generateForm d (n-1)
--             return (fa:fb)

-- Excercise 5
sub :: Form -> Set Form
sub (Prop x) = Set [Prop x]
sub (Neg f) = unionSet (Set [Neg f]) (sub f)
sub f@(Cnj [f1,f2]) = unionSet ( unionSet (Set [f]) (sub f1)) (sub f2)
sub f@(Dsj [f1,f2]) = unionSet ( unionSet (Set [f]) (sub f1)) (sub f2)
sub f@(Impl f1 f2) = unionSet ( unionSet (Set [f]) (sub f1)) (sub f2)
sub f@(Equiv f1 f2) = unionSet ( unionSet (Set [f]) (sub f1)) (sub f2)

-- Exercise 6


main :: IO ()
main = do
  putStrLn "===Excercise 1==="
  print testContradiction
  print testTautology
  print testEntails
  print testEquiv
  putStrLn "\n===Excercise 2==="
  print testParse
  print testParse2
  print testParse3
  print testParse4
  print testParse5
  print testParse6
  print testParse7
  putStrLn "\n===Excercise 3==="
  print cnfTest
  print cnfTest2
  putStrLn "\n===Excercise 4==="
  -- print
  putStrLn "\n===Excercise 5==="
  putStrLn "\n===Excercise 6==="
