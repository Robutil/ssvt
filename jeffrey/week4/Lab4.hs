module Lab4 where

import Data.List
import System.Random
import Test.QuickCheck
import SetOrd

-- Excercise 1
-- I was in doubt on how to generate the integers as random as possible.
-- My implementations either generated only extremely high integers or too small.
-- Therefore I combined a smal and big integers generation, but this does not
-- make it really random anymore.

randomIntBig :: IO Int
randomIntBig = getStdRandom random

randomIntSmall :: Int -> IO Int
randomIntSmall x = getStdRandom $ randomR((-x), x)

makeRandomSet :: Int -> Set Int -> IO (Set Int)
makeRandomSet limit set@(Set xs) = do
    if (length xs) == limit then
        return set
    else if (length xs) < (limit `div` 2) then do
        x <- randomIntSmall 100
        tmp_set <- makeRandomSet limit (insertSet x set)
        return tmp_set
    else do
        x <- randomIntBig
        tmp_set <- makeRandomSet limit (insertSet x set)
        return tmp_set

makeRandomSetInt :: Int -> IO (Set Int)
makeRandomSetInt limit = makeRandomSet limit emptySet


-- Excercise 2
-- gives set with intersections of two sets
setIntersection :: (Ord a) => Set a -> Set a -> Set a
setIntersection (Set a) (Set b) = list2set [x | x <- a, x `elem` b]

-- gives set with union of two sets
setUnion :: (Ord a) => Set a -> Set a -> Set a
setUnion (Set a) (Set b) = list2set $ a ++ b

-- gives set with difference of two sets
setDiff :: (Ord a) => Set a -> Set a -> Set a
setDiff (Set a) (Set b) = list2set $ (a \\ b) ++ (b \\ a)

testSetIntersection = setIntersection (list2set [1,2,3]) (list2set [1,3,4])
testSetUnion = setUnion (list2set [1,3,6]) (list2set [1,3,4,5])
testSetDiff = setDiff (list2set [1,2,3]) (list2set [1,4])

-- Excercise 3
type Rel a = [(a,a)]

-- swap the pair
swapRel :: (a,b) -> (b,a)
swapRel (a,b) = (b,a)

symClos :: Ord a => Rel a -> Rel a
symClos [] = []
symClos (x:xs) = [x, (swapRel x)] ++ symClos xs

testSymClos = symClos [(1,2), (3,4), (6,7)]

-- Excercise 4
-- isSerial :: Eq a => [a] -> Rel a -> Bool


-- Excercise 5
infixr 5 @@

-- This function returns part of the transitive closure of the current relation
-- nub makes sure there are no duplicates
(@@) :: Eq a => Rel a -> Rel a -> Rel a
r @@ s = nub [ (x,z) | (x,y) <- r, (w,z) <- s, y == w ]

-- give the transitive closure of a relation
trClos :: Ord a => Rel a -> Rel a
trClos rel =
    if rel /= nub (rel ++ (rel @@ rel)) then
        trClos (nub (rel ++ (rel @@ rel)))
    else
        rel

testTrClos = trClos [(1,2),(2,3)]


-- Bonus excercise 8
-- type Var = String
--
-- data Expr = I Integer
--     | V Var
--     | Add Expr Expr
--     | Subtr Expr Expr
--     | Mult Expr Expr
--     deriving (Eq,Show)
--
-- data Condition = Prp Var
--     | Eq Expr Expr
--     | Lt Expr Expr
--     | Gt Expr Expr
--     | Ng Condition
--     | Cj [Condition]
--     | Dj [Condition]
--     deriving (Eq,Show)
--
-- data Statement = Assgn Var Expr
--     | Cond Condition Statement Statement
--     | Seq [Statement]
--     | While Condition Statement
--     deriving (Eq,Show)




main :: IO ()
main = do
  putStrLn "\n===Excercise 1==="
  putStrLn "\n===Excercise 2==="
  print testSetIntersection
  print testSetUnion
  print testSetDiff
  putStrLn "\n===Excercise 3==="
  print testSymClos
  putStrLn "\n===Excercise 5==="
  print testTrClos
  putStrLn "\n"
