module Lab1 where
import Data.List
import Test.QuickCheck

infix 1 -->

(-->) :: Bool -> Bool -> Bool
p --> q = (not p) || q

forall :: [a] -> (a -> Bool) -> Bool
forall = flip all

-- exercise 1:
-- (15 min.)
-- Sum of powers of two
sumSquares :: Integer -> Integer
sumSquares n = sum $ map (^2) [1..n]

sumSquares' :: Integer -> Integer
sumSquares' n = n*(n+1)*(2*n +1) `div` 6

-- QuickCheck on the sum of squares formulas
testSquares :: Integer -> Bool
testSquares n = let a = abs n in sumSquares a ==  sumSquares' a

-- Sum of powers of three
sumThree :: Integer -> Integer
sumThree n = sum $ map (^3) [1..n]

sumThree' :: Integer -> Integer
sumThree' n = (n*(n+1) `div` 2)^2

-- QuickCheck on the sum of powers of three formulas
testThree :: Integer -> Bool
testThree n = let a = abs n in sumThree a ==  sumThree' a

-- exercise 2:
-- (40 min.)
-- Sum sets in powerset
sumPowerset :: Integer -> Int
sumPowerset n = length (subsequences [1..n])

-- Sum sets with power of two
sumPowerset' :: Integer -> Int
sumPowerset' n = 2^n

-- QuickCheck between two statements for powerset length
testPowerset :: Integer -> Bool
testPowerset n = n >= 0 && n < 25 --> sumPowerset n ==  sumPowerset' n

-- The property is hard to test, because the length of sets changes with
-- a power of n. Meaning that if n = 20 there are more than a milion sets that
-- the function sebsequences must generate. This takes a lot of time and it is
-- therefore hard to test if we want to test an extensive amount of cases.

-- The test is testing if the amount of sets that subsequences generates equals
-- to the formula 2^n. So we are partly checking for the mathematical fact that
-- the two statements give the same result and also if subsequences does what
-- it claims to do.

-- exercise 3:
--  Sum of permuations of list
perms :: Integer -> Integer
perms n = toInteger $ length $ permutations [1..n]

perms' :: Integer -> Integer
perms' n = foldl (*) 1 [1..n]

-- Test the two sum permuation functions
testPerms :: Integer -> Bool
testPerms n = n >= 0 && n < 11 --> perms n == perms' n

-- Just like the powerset testing in exercise 2, this property is also hard to
-- test in a way. The reason is that the amount of permutation is equal to the
-- factorial of the amount of elements in the list. Which means that if there
-- is a list consisting of 6 elements there are 720 permutations/combinations.

-- Again we are testing two things. First whether the factorial of n-elements
-- is equal to the length of the permutations of a list with n elements.
-- Consequently we are also testing if the permutations function does what we
-- would mathematically expect.

-- exercise 4:
-- (60 min.)
-- Indicates if value is a prime
prime :: Integer -> Bool
prime n = n > 1 && all (\ x -> rem n x /= 0) xs
  where xs = takeWhile (\ y -> y^2 <= n) primes

-- Generates infinite list of primes
primes :: [Integer]
primes = 2 : filter prime [3..]

-- Reverse the independent digits of a value
reversal :: Integer -> Integer
reversal = read . reverse . show

-- Get all primes for which the reverse is also prime
reversiblePrimes' :: [Integer] -> [Integer]
reversiblePrimes' [] = []
reversiblePrimes' (h:ps) =
    if prime $ reversal(h) then
        [h] ++ reversiblePrimes'(ps)
    else
        reversiblePrimes'(ps)

reversiblePrimes :: [Integer]
reversiblePrimes = reversiblePrimes'(takeWhile (<10000) primes)

-- exercise 5:
-- Find smallest prime that is a sum of 101 consecutive primes
sumSubsPrime :: [Integer] -> Integer
sumSubsPrime (x:xs) =
    if prime $ sum $ x:take 100 xs then
        sum $ x:take 100 xs
    else
        sumSubsPrime xs

resultSumSubsPrime :: Integer
resultSumSubsPrime = sumSubsPrime primes

-- The answer that I found is 37447. To check if this answer is correct we must
-- check if 37447 is a prime, which it is. Also the list with the consecutive
-- primes must be checked if each element is a prime.

-- exercise 6
-- Refute a conjecture
refuteCon :: [Int] -> Integer
refuteCon (x:xs) =
    if prime $ (product(take x primes)) + 1 then
        refuteCon xs
    else
        product(take x primes) + 1

resultRefuteCon :: Integer
resultRefuteCon = refuteCon [0..]

-- The smallest counterexample is the list with 6 consecutive primes starting
-- from prime number 2. This gives the result 30031 which is not a prime.

-- Question 7
luhnDouble :: Integer -> Integer
luhnDouble n = if n*2 > 9
  then (2*n)-9
    else 2 * n

luhnDouble' :: Integer -> [Integer] -> [Integer]
luhnDouble' n [] = []
luhnDouble' n (x:xs) = if n == 0 then
    x : luhnDouble' 1 xs
    else luhnDouble x : luhnDouble' 0 xs

-- Lab 1 Question 8
data Boy = Matthew | Peter | Jack | Arnold | Carl
           deriving (Eq,Show)
boys = [Matthew, Peter, Jack, Arnold, Carl]

-- initializing accutations between the boys
accuses :: Boy -> Boy -> Bool
accuses Matthew Carl = False
accuses Matthew Matthew = False
accuses Matthew _ = True
accuses Peter Matthew = True
accuses Peter Jack = True
accuses Jack boy = not (accuses Matthew boy) && not (accuses Peter boy)
accuses Arnold boy = (accuses Matthew boy) /= (accuses Peter boy)
accuses Carl boy = not (accuses Arnold boy)
accuses _ _ = False

accusers :: Boy -> [Boy]
accusers boy = [x | x <- boys, accuses x boy]

-- find guilty boy by searching who is accused by three others.
-- find honest guy by searching who accused the guilty boy
guilty, honest :: [Boy]
guilty = [x | x <- boys, length(accusers x) == 3]
honest = accusers (head guilty)

-- euler 10
eulerPrime = sum[x | x <- [2..2000000], prime x]

-- Executing test and result functions
main = do
    quickCheck testSquares
    quickCheck testThree
    quickCheck testPowerset
    quickCheck testPerms
    print reversiblePrimes
    print resultSumSubsPrime
    print resultRefuteCon
