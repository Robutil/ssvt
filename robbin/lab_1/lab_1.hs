import Data.Function
import Data.List
import Test.QuickCheck

-- Lab provided utilities
(-->) :: Bool -> Bool -> Bool
p --> q = (not p) || q
-- End of lab provided utilities

-- Lab 1 Question 1 Part 1
sumSquares :: Integer -> Integer
--sumSquares n = sum ( map (^2) [1..n] )
sumSquares n = sum [x^2 | x <- [1..n]]

sumSquaresProperty :: Integer-> Integer
sumSquaresProperty n = ((n * (n + 1) * (2 * n + 1)) `div` 6)

testSumSquares :: Integer -> Bool
testSumSquares n = (n>=0) --> (sumSquares n == sumSquaresProperty n)

-- Lab 1 Question 1 Part 2
sumKubes :: Integer -> Integer
sumKubes n = sum [x^3 | x <- [1..n]]

sumKubesProperty :: Integer -> Integer
sumKubesProperty n = ((n * (n+1)) `div` 2) ^ 2

testSumKubes :: Integer -> Bool
testSumKubes n = (n>=0) --> (sumKubes n == sumKubesProperty n)

-- Lab 1 Question 2
setLength :: Integer -> Integer
setLength n = fromIntegral (length [1..n])

powerSetLength :: Integer -> Integer
powerSetLength n = fromIntegral (length (subsequences [1..n]))

powerSetLengthProperty :: Integer -> Integer
powerSetLengthProperty n = 2^n

testPowerSetLength :: Integer -> Bool
testPowerSetLength n = (n>=0 && n<30) --> (powerSetLength n == powerSetLengthProperty n)
-- The tests are limited to small numbers because subsequences is a very expensive operation; the tests will take a really long time
-- As for what we are testing; since we have proven in the workshop using induction that the premise is valid we are testing the implementations
-- of subsequences and length. Expensive operations are hard to test (exhaustively) because we do not want to wait a long time.

-- Lab 1 Question 3
-- Let's use Data.List permutations
-- TODO: Use lists as input instead of Integers
permutationLength :: Integer -> Integer
permutationLength n = fromIntegral $ length $ permutations [1..n]

permutationLengthProperty :: Integer -> Integer
permutationLengthProperty n = product [1..n]

testPermutationLength :: Integer -> Bool
testPermutationLength n = (n>0 && n<10) --> (permutationLength n == permutationLengthProperty n)
-- The tests are once again limited to small numbers because permutations is a very expensive operation; the tests will take a really long time
-- Since the property is once again proven by induction we are testing the length, product and permutation functions.

-- Lab 1 Question 4
reversal :: Integer -> Integer
reversal =  read . reverse . show

prime :: Integer -> Bool
prime n = n > 1 && all (\ x -> rem n x /= 0) xs where xs = takeWhile (\ y -> y^2 <= n) primes

primes :: [Integer]
primes = 2 : filter prime [3..]
somePrimes :: Integer -> [Integer]
somePrimes n = take (fromIntegral n) primes

isReversePrime :: Integer -> Bool
isReversePrime n = prime $ reversal n

findReversePrimes :: Integer -> [Integer]
findReversePrimes n = [x | x <- [1..n], prime x, isReversePrime x] -- x <- (somePrimes n)
-- Optimalizations include: lookup isReversePrime in shared prime list
-- Test by checking individual primes and their reverse prime for validity, and checking a few length of sets
findReversePrimesBelowTenThousand = findReversePrimes 10000

-- Lab 1 Question 5
consecutivePrimes :: [Integer] -> [Integer]
consecutivePrimes (x:xs) = if (prime (sum (x:(take 100 xs)))) then x:(take 100 xs) else consecutivePrimes xs

premiseAllPrime :: [Integer] -> Bool
premiseAllPrime xs = all prime xs

consecutivePrimesResult = consecutivePrimes primes
mustAllBePrime = premiseAllPrime consecutivePrimesResult
otherConSecChecks = length consecutivePrimesResult == 101 && prime ( sum consecutivePrimesResult )

-- Because we iteratively take larger primes, the first 101 that form a prime number is the smallest. We can show that it is capable
-- of finding the smallest if we show that it can find a bigger one. This can be easily done with removing the first from the found 101
-- and adding the infinite prime list after that (little bit more tricky to find the indexes).

-- Lab 1 Question 6
-- Refute (p1 * p2 * ... * pn) + 1 == prime
-- Take all possible ordered sets that have a different length but smaller than `take x primes`
-- Check, in order by size, if premise is invalid. If so return, else continue.
-- If subsets are exhausted increase x by one, repeat (preferable cache previous iteration results)
refutePrimes :: Int -> [Integer] -> [Integer]
refutePrimes n xs = if (refutePrimesPremise (take n xs)) then (take n xs) else refutePrimes (n+1) xs
-- To find the smallest; call refutePrimes with every subset (even if the primes are larger). Keep smallest.
-- e.g. refutePrimes 1 primes, refutePrimes x (drop y primes), ...
refutePrimesPremise xs = not (prime ((product xs) + 1))

-- Lab 1 Question 7
luhn :: Integer -> Bool
luhn x = ((calculateCheckDigit (explodeInteger x) 0 0) `mod` 10) == 0
explodeInteger :: Integer -> [Integer]
explodeInteger 0 = []
explodeInteger x = explodeInteger(x `div` 10) ++ [x `mod` 10]

luhnAlgo :: Integer -> Integer
luhnAlgo x | a > 9 = (a-9)
           | otherwise = a
           where a = x*2

calculateCheckDigit :: [Integer] -> Integer -> Integer -> Integer
calculateCheckDigit [] _ s = s
calculateCheckDigit (x:xs) n s = if (even n) then (calculateCheckDigit xs (n+1) (s+x)) else (calculateCheckDigit xs (n+1) (s+(luhnAlgo x)))

luhnCheckDigit :: Integer -> Integer
luhnCheckDigit x = ((calculateCheckDigit (explodeInteger x) 0 0) * 9) `mod` 10

-- https://7labs.io/tips-tricks/check-validity-of-credit-card-number.html
isAmericanExpress, isMaster, isVisa :: Integer -> Bool
isAmericanExpress n = (length (explodeInteger n) == 15) && (luhn n)
isMaster n = (length (explodeInteger n) == 16) && (luhn n) && (show n !! 0) == '5'
isVisa n = (length (explodeInteger n) == 16) && (luhn n) && (show n !! 0) == '4'

-- This should be a valid number, but it is rejected...
-- example = isVisa 4417123456789113

-- Lab 1 Question 8
data Boy = Matthew | Peter | Jack | Arnold | Carl
           deriving (Eq,Show)
boys = [Matthew, Peter, Jack, Arnold, Carl]

accuses :: Boy -> Boy -> Bool
accuses Matthew Carl = False
accuses Matthew Matthew = False
accuses Matthew _ = True
accuses Peter Matthew = True
accuses Peter Jack = True
accuses Jack someBoy = not (accuses Matthew someBoy) && not (accuses Peter someBoy)
accuses Arnold someBoy = (accuses Matthew someBoy) /= (accuses Peter someBoy)
accuses Carl someBoy = not (accuses Arnold someBoy)
accuses _ _ = False

accusers :: Boy -> [Boy]
accusers someBoy = [x | x <- boys, accuses x someBoy]

guilty, honest :: [Boy]
guilty = [x | x <- boys, length(accusers x) >= 3]
honest = accusers (head guilty)

-- Main execution
main = do
  quickCheck testSumSquares
  quickCheck testSumKubes
  quickCheck testPowerSetLength
  quickCheck testPermutationLength
  print findReversePrimesBelowTenThousand
  print consecutivePrimesResult
  print (refutePrimes 1 primes)
  print (luhn 79927398713)
  print guilty
  print honest
