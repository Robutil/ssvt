module Lab3 where

import Data.List
import System.Random
import Test.QuickCheck
import Lab3Helper
import SetOrdHelper

-- Lab 3 Question 1
-- Time spend ~30 minutes
contradiction :: Form -> Bool
contradiction f = not (satisfiable f)
testContradiction = contradiction (Cnj [Prop 1, Neg(Prop 1)]) -- (p && !p)

tautology :: Form -> Bool
tautology f = all (\v -> evl v f) (allVals f)
testTautology = tautology (Dsj [Prop 1, Neg(Prop 1)]) -- (p || !p)

entails :: Form -> Form -> Bool
entails fa fb = all (\v -> if (evl v fa) then (evl v fb) else True) (allVals fa)
testEntails = entails (Impl (Prop 1) (Prop 2)) (Impl (Neg (Prop 2)) (Neg (Prop 1))) -- (a -> b) <=> (!b -> !a)

equiv :: Form -> Form -> Bool
equiv fa fb = (entails fa fb) && (entails fb fa)
testEquiv = equiv (Impl (Prop 1) (Prop 2)) (Impl (Neg (Prop 2)) (Neg (Prop 1))) -- (a -> b) <=> (!b -> !a)

-- Lab 3 Question 2
-- Grammar:
--   * == &&
--   + == ||
--   - == !
-- Time spend ~30 minutes, not happy with my test cases because I want to auto generate them but I am frustrated with
-- Haskell and the parse function so I will leave it for now, will come back to it if I feel like it.

-- Thought process: let's manually play around a bit to see edge cases
-- When I feel comfortable maybe think about automating it.
testParse1 = parse "*(5+)" == [] -- Expected, invalid Form
testParse2 = parse "* 1 + 2 - 3" == [] -- Unexpected, parser should be able to find out precendence without brackets
testParse3 = parse "*(1+(2-3))" /= [] -- Expected, brackets all around should be easy
testParse4 = parse "*((1) + (2-3) )" == [] -- Unexpected, extra brackets should have no effect, but it breaks the parser
testParse5 = parse "*(+(2-3) 1)" /= [] -- Expected, just flipped arguments to &&
testParse6 = parse "2" /= [] -- Good
testParse7 = parse "(2)" == [] -- Bad
testParse8 = parse "-(2)" == [] -- Bad
testParse9 = parse "-+(2 1)" /= [] -- Ok
-- Okay, So we can only use brackets in combination with Dsj and Cnj
-- Let's go a little bit deeper and play with ==> and <=>
testParse10 = parse "1==>2" /= [] -- Result is [1] but I expected [1==>2]
testParse11 = parse "(==> 1 2)" == [] -- Trying to find out how to use the imply token
testParse12 = parse "+(1 2 6)(==>+(1 3)ssdasdsa)" /= [] -- Bad, would expect to see error due to illegal tokens
testParse13 = parse "(1==>2)" /= [] -- Good, this is how it should be used. Very confusing style compared to * and +

-- Lab 3 Question 3
-- Bout 40 mins

formToCnf :: Form -> Form
-- Remove brackets
formToCnf (Prop a) = Prop a

-- Remove double negate
formToCnf (Neg ( Neg ( a ) )) = formToCnf a

-- De Morgan's Law to get rid of negates outside of Cnj and Dsj
formToCnf (Neg (Cnj [a, b])) = formToCnf (Dsj [Neg a, Neg b]) 
formToCnf (Neg (Dsj [a, b])) = formToCnf (Cnj [Neg a, Neg b])

-- Distributive law
formToCnf (Dsj [a, Cnj [b, c]]) = formToCnf (Cnj [Dsj [a, b], Dsj [a, c]])

-- Recursion
formToCnf (Dsj [a, b]) = Dsj [formToCnf a, formToCnf b]
formToCnf (Cnj [a, b]) = Cnj [formToCnf a, formToCnf b]
formToCnf a = a

toCnf form = formToCnf (arrowfree form)

cnfTest = formToCnf (arrowfree ((Impl (Neg (Prop 2)) (Neg (Prop 1)))))
cnfTest1 = formToCnf (arrowfree (Equiv (Prop 1) (Prop 2))) -- *(+(*(1 2) -1) +(*(1 2) -2)) ==> ((a&&b)||!a) && ((a&&b)||!b)
-- TODO: More tests, but this seems okay
-- TODO: WIP, this is actually not okay. We should not have nested Dsj/Cnj.

-- Lab 3 Question 4
-- Generate bare property 
val :: Integer -> [Char]
val x | x `mod` 2 == 0 = show (x+1)
      | otherwise = "-" ++ (show (x+1))

-- Generate simplistic property
generateForm n form | (length form) > 50 = form
    | x < 0 = generateForm (abs n) form
    | x == 0 = generateForm (n-2) (form ++ " +(" ++ (val n) ++ " " ++ (val (n-1)) ++ ")")
    | x == 1 = generateForm (n-2) (form ++ " *(" ++ (val n) ++ " " ++ (val (n-1)) ++ ")")
    | x == 2 = generateForm (n-2) (form ++ " (" ++ (val n) ++ " ==> " ++ (val (n-1)) ++ ")")
    | x == 3 = generateForm (n-2) (form ++ " (" ++ (val n) ++ " <=> " ++ (val (n-1)) ++ ")")
    where x = n `mod` 4

-- Generate nested properties
formGen n form | (length form) > 100 = form -- Recursion is infinite for some reason...
    | x < 0 = formGen (abs n) form
    | x == 0 = formGen (n-1) (" *(" ++ (generateForm n "") ++ " " ++ (generateForm (n-1) "") ++ ")")
    | x == 1 = formGen (n-1) (" +(" ++ (generateForm n "") ++ " " ++ (generateForm (n-1) "") ++ ")")
    | x == 2 = formGen (n-1) (" +(" ++ (generateForm n "") ++ " " ++ (generateForm (n-1) "") ++ ")")
    | x == 3 = formGen (n-1) (" +(" ++ (formGen (n-1) "") ++ " " ++ (generateForm (n-1) "") ++ ")")
    | x == 4 = formGen (n-1) (" (" ++ (formGen (n-2) "") ++ " ==> " ++ (formGen (n-2) "") ++ ")")
    | x == 5 = formGen (n-1) (" (" ++ (formGen (n-3) "") ++ " <=> " ++ (formGen (n-3) "") ++ ")")
    where x = n `mod` 6

testCnf :: Integer -> Bool
testCnf x = let form = parse (formGen x "") !! 0 in equiv form (toCnf form)

-- Lab 3 Question 5
type Name = Int

sub :: Form -> Set Form
sub (Prop x) = Set [Prop x]
sub (Neg f) = unionSet (Set [Neg f]) (sub f)
sub f@(Cnj [f1,f2]) = unionSet ( unionSet (Set [f]) (sub f1)) (sub f2)
sub f@(Dsj [f1,f2]) = unionSet ( unionSet (Set [f]) (sub f1)) (sub f2)
sub f@(Impl f1 f2) = unionSet ( unionSet (Set [f]) (sub f1)) (sub f2)
sub f@(Equiv f1 f2) = unionSet ( unionSet (Set [f]) (sub f1)) (sub f2)

-- My first test was with generated forms. However, a Conjunction or Disjunction of multiple elements
-- is not handled correctly by `sub`. E.g. the following:
--  *** Exception: +(*(-2 1) (-0<=>-1) *(--2 -3) (--4<=>-5) *(--6 -7) +(1 -0) (-5==>--6) +(-7 --8))
-- This error message is visible when setting an exhaustive case: sub o = error(show o)
-- It makes sense to have multiple Conjunctions and Disjunctions, especially when dealing with CNF
-- The root cause of the issue is found in the signature of sub where Cnj/Dsj [f1, f2].
testPresent :: Integer -> Bool
testPresent x = let form = parse (formGen x []) !! 0 in length [f | f <- (explodeForm form []), not (inSet f (sub form))] == 0

-- Calculates length of a given Set Form
subLengthHelper :: Set Form -> Integer -> Integer
subLengthHelper (Set []) n = n
subLengthHelper (Set (x:xs)) n = subLengthHelper (Set xs) (n+1)
subLength :: Set Form -> Integer
subLength xs = subLengthHelper xs 0 

-- New generator that only generates very simplistic forms that sub can handle. Parse will make sure
-- that we do not have nesting. (Because we miss outermost wrapping). Also note that we do not negate anymore
-- because then our explode length will no longer match.
generateForm2 n form | (length form) > 50 = form
    | x < 0 = generateForm (abs n) form
    | x == 0 = generateForm (n-2) (form ++ " +(" ++ (show n) ++ " " ++ (show (1)) ++ ")")
    | x == 1 = generateForm (n-2) (form ++ " *(" ++ (show n) ++ " " ++ (show (n-1)) ++ ")")
    | x == 2 = generateForm (n-2) (form ++ " (" ++ (show n) ++ " ==> " ++ (show (n-1)) ++ ")")
    | x == 3 = generateForm (n-2) (form ++ " (" ++ (show n) ++ " <=> " ++ (show (n-1)) ++ ")")
    where x = n `mod` 4
-- We can improve it a little bit if we do allow nesting for Cnj/Dsj sub parameters (in this recursion). This
-- would limit the parameters for sub Cnj/Dsj to two. However, we don't want to test a broken implementation.
-- It's better to fix sub, and use the full generator.

manualSubTest1 = subLength (sub (parse "--1" !! 0)) == 3
manualSubTest2 = subLength (sub (parse "-1" !! 0)) == 2 
manualSubTest3 = subLength (sub (parse "---1" !! 0)) == 4 

testLength :: Integer -> Bool
testLength x = let form = parse (generateForm2 (abs x) "") !! 0 in length (explodeForm form []) == fromIntegral ( subLength (sub form))


explodeForm :: Form -> [Form] -> [Form]
explodeForm (Prop x) forms = forms ++ [Prop x]
explodeForm (Neg x) forms = forms ++ [Neg x]
explodeForm (Cnj xs) forms = forms ++ [Cnj []] ++ (concat [explodeForm y [] | y <- xs])
explodeForm (Dsj xs) forms = forms ++ [Dsj []] ++ (concat [explodeForm y [] | y <- xs])
explodeForm (Impl x y) forms = forms ++ [(Impl x y)] ++ (explodeForm x []) ++ (explodeForm y [])
explodeForm (Equiv x y) forms = forms ++ [(Equiv x y)] ++ (explodeForm x []) ++ (explodeForm y [])


-- Lab 3 Question 6
-- Spend approx 1 hour, no generation yet. This exercise was not frustrating, actually mildly enjoyable.
-- CNF notation allows us to not expect nested Dsj and Conj

-- We only Handle Prop and Negate here. These are the only legal arguments.
formToInt :: Form -> Int
formToInt (Prop a) = read (show a)
formToInt (Neg (Prop a)) = -(formToInt (Prop a))

-- We only handle Dsj, Neg, and Prop here. These are the only legal arguments
formToIntArr :: Form -> [Int]
formToIntArr (Prop a) = [formToInt (Prop a)]
formToIntArr (Neg (Prop a)) = [-formToInt (Prop a)]
formToIntArr (Dsj xs) = [formToInt x | x <- xs]
formToIntArr _ = [0] -- Error case

-- Given input is a Conjunction or any in [Dsj, Neg, Prop], which is handled by formToIntArr
cnf2cls :: Form -> [[Int]]
cnf2cls (Cnj xs) = [formToIntArr x | x <- xs]  
cnf2cls a = [formToIntArr a]

parseHelper :: String -> Form
parseHelper s = parse s !! 0

testClsHelper :: String -> [[Int]] -> Bool
testClsHelper formula expectation = cnf2cls (parseHelper formula) == expectation 
testCls1 = testClsHelper "5" [[5]]
testCls2 = testClsHelper "-99" [[-99]]
testCls3 = testClsHelper "*(4 +(5 6))" [[4], [5, 6]]
testCls4 = testClsHelper "*(4 +(5 -6))" [[4], [5, -6]]

-- Properties:
--   - Same number of names
--   - Number of sublists is the same as length AND
--   - Number of element in sublist are the same as length OR

-- Generating test cases:
--   - Use unique names throughout to support properties
--   - Base case (x || -x) is handled by manual test (testCls1, testCls2)
--   - Use all subset combinations as [+(subset) remainder] where remainer can be a +() or base case
--   - Wrap subset combinations into Conjuncture using "*(" + " ".join(subsets) + ")"

main :: IO ()
main = do
    putStrLn "===Exercise 1==="
    print (testContradiction) 
    print (testTautology) 
    print (testEntails)
    print (testEquiv)
    putStrLn "===Exercise 2==="
    print (testParse1)
    print (testParse2)
    print (testParse3)
    print (testParse5)
    print (testParse6)
    print (testParse7)
    print (testParse8)
    print (testParse9)
    print (testParse10)
    print (testParse11)
    print (testParse12)
    print (testParse13)
    putStrLn "===Exercise 4==="
    quickCheck testCnf
    putStrLn "===Exercise 6==="
    print (testCls1)
