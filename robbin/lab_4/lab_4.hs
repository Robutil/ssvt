{- # Notes on Haskell Road
 - ## Naming conventions
 - The tendency to use single letters for variable names is a common theme in the book, which greatly increases
 - the complexity for the reader (examples include: x, y, n). Often mental mapping is relied upon. Even in cases
 - where the mental mapping is small, is is easily avoidable. E.g. consider the following example:
 -
 -   even2 = [ 2*n | n <- naturals ] 
 -   -- Could also be written as:
 -   evenNumbers = [ 2*n | n <- naturalNumbers ] 
 -
 - I also argue that the usage of `n` in this case is actually fine. It is clear where the n comes from, and it
 - is a common iterator for numbers (just like, x, y, i, j). However, the problem arises when random letters are
 - used as parameters. E.g.
 -
 -   f (x:xs) n y = if (even n) then (f xs (n+1) (y+x)) else (f xs (n+1) (y+(l x)))
 -
 - If you are wondering, this is the Luhn Algorithm from week 1, but with terrible variable names. Some of the brackets
 - above are surely not required, but without them it would read even more messier because then the reader has to mentally
 - map the brackets in the right place.
 -
 - Also, names like `run` and `funny` are not descriptive names, it also doesn't help to sometimes switch to greek. Finally,
 - naming conventions should be consistent. and not use underscores if the default in haskell is pascalCase. The inverse also
 - occurs when sets are suddely depicted by an ALLCAPS format, e.g. `A` or `B`. If you need formatting conventions to keep your
 - variable names apart, then your name is probably not very descriptive.
 -
 - ## Types
 - In Haskell the type for a function is often relied upon to show what the function means. Whilst it does provide you with a
 - clear requirement of what parameters and result a function gives it is not a truth to what operation the function might perform.
 - In fact, the type system lures you into a false sense of security that you know the inner workins of a function because you know
 - the type. E.g. consider the following base operator:
 -   
 -   (:) :: a -> [a] -> [a]
 -   -- and this implementation
 -   (:) _ items = items
 -
 - The implementation of the function adheres to the type that was declared. But does it do what you want? This is of course a problem.
 - It can be solved with documentation, a short description of what the function does. A descriptive function name can also go a long 
 - way. Finally, the real truth is of course the implememtation, but with only single letter variables it is often tedious to find out
 - exactly what a function does, so the hint that the type of the function gives us will have to do...
 -}

import SetOrdHelper
import System.Random
import Test.QuickCheck
import Data.List

-- Lab 4 question 1
seed = randomIO :: IO Int -- Wrap IO here so we can keep our generators pure

infiniteRandomInts :: Int -> [Int]
infiniteRandomInts seed = randoms (mkStdGen seed) :: [Int] -- Maybe it makes more sense to limit the range e.g. -100 to 100

randomInts :: Int -> Int -> [Int]
randomInts seed max = let xs = infiniteRandomInts seed in take (head xs `mod` max) (tail xs)

randomInt :: Int -> Int
randomInt seed = head (infiniteRandomInts seed)

randInt :: Int -> Int -> Int -> Int
randInt _ 0 0 = 0
randInt seed min 0 = (-1) * (randInt seed 0 ((-1)*min))
randInt seed min max = let number = randomInt seed in if number < min then (min + (abs number)) `mod` max else number `mod` max

-- Generate a set of random integers of random length (but max length == 100)
randomIntSet :: Int -> Set Int
randomIntSet seed = list2set (randomInts seed 100)
-- The random generation could be more accurate if we are smarter about the ranges. E.g. We want to try a couple of small numbers
-- and some big numbers. But the current generation mostly generates really large or small numbers which are unlikely to find any
-- edge cases (apart from those concerning big numbers). This is of course, where QuickCheck comes in.

-- Generates 100 random length sets with random numbers
randomIntSets :: Int -> [Set Int]
randomIntSets seed = [randomIntSet (seed + x) | x <- [1..100]]

emptyIntSet :: Set Int
emptyIntSet = list2set []

-- Empty set is always a subset of any set
intSetProperty :: [Int] -> Bool
intSetProperty xs = subSet emptyIntSet (list2set xs) 

testRandomIntSet :: IO () 
testRandomIntSet = quickCheck intSetProperty

manualTestRandomIntSet :: Bool
manualTestRandomIntSet = all (==True) [intSetProperty (randomInts x 100) | x <- [1..100]]

quickTestRandomIntSet :: [[Int]] -> Bool
quickTestRandomIntSet xs = all (==True) [intSetProperty x | x <- xs]

-- Made the below before I knew about `nubs`
removeDuplicates :: (Eq a) => [a] -> [a]
removeDuplicates [] = []
removeDuplicates (x:xs) = if x `elem` xs then removeDuplicates xs else x:(removeDuplicates xs)

removeDuplicatesFromSet :: (Ord a) => Set a -> Set a
removeDuplicatesFromSet (Set xs) = list2set (removeDuplicates xs) 

-- Lab 4 question 2
-- Implement:
--   - intersection
--   - union
--   - difference
-- For datatype `Set`

setIntersection :: (Ord a) => Set a -> Set a -> Set a
setIntersection (Set a) (Set b) = list2set (removeDuplicates [a | a <- a, a `elem` b])

setUnion :: Ord a => Set a -> Set a -> Set a
setUnion (Set a) (Set b) = list2set (removeDuplicates (a ++ b))

setDifference :: Ord a => Set a -> Set a -> Set a
setDifference (Set a) (Set b) = list2set [x | x <- a, not (x `elem` b)]

manualTestSetIntersection1 :: Bool
manualTestSetIntersection1 = let sample = randomIntSet 1 in (setIntersection sample sample) == sample

randomSplitSet :: Ord a => Int -> Set a -> (Set a, Set a)
randomSplitSet seed (Set a) = let splitAt = randInt seed 0 (length a) in (list2set (take splitAt a), list2set (drop splitAt a))

-- Split set at random index and then add a random amount of elements from the second half to the first half. The intersection
-- should be the random elements that were chosen from the second half
manualPropertyIntersection :: Ord a => Int -> Set a -> Bool
manualPropertyIntersection seed a = let (Set b, Set c) = randomSplitSet seed a in setIntersection (list2set (b ++ (take (randInt seed 0 (length c))) c)) (list2set c) == list2set (take (randInt seed 0 (length c)) c)

-- The union of a set split at a random index should result in the same set
manualPropertyUnion :: Ord a => Int -> Set a -> Bool
manualPropertyUnion seed a = let (b, c) = randomSplitSet seed a in (setUnion b c) == a

-- Split set at random index and then add a random amount of elements from the second half to the first half. The difference
-- should be the first half but without the random amount of elements from the second half.
manualPropertyDifference :: Ord a => Int -> Set a -> Bool
manualPropertyDifference seed a = let (Set b, Set c) = randomSplitSet seed a in setDifference (list2set (b ++ (take (randInt seed 0 (length c))) c)) (list2set c) == list2set b

-- Tests 100 random Sets with own generator
manualTestSetIntersection2 seed = all (==True) [manualPropertyIntersection (seed + x) (randomIntSet (seed + x)) | x <- [1..100]]
manualTestSetUnion2 seed = all (==True) [manualPropertyUnion (seed + x) (randomIntSet (seed + x)) | x <- [1..100]]
manualTestSetDifference2 seed = all (==True) [manualPropertyDifference (seed + x) (randomIntSet (seed + x)) | x <- [1..100]]

-- QuickTest tests
testIntersection :: (Int, [Int]) -> Bool
testIntersection (seed, a) = manualPropertyIntersection ((abs seed) + 1) (list2set a)

testUnion :: (Int, [Int]) -> Bool
testUnion (seed, a) = manualPropertyUnion ((abs seed) + 1) (list2set a)

testDifference :: (Int, [Int]) -> Bool
testDifference (seed, a) = manualPropertyDifference ((abs seed) + 1) (list2set a)

-- Lab 4 question 3
type Rel a = [(a,a)]

symClos :: Ord a => Rel a -> Rel a
symClos [] = []
symClos ((a,b):xs) = [(a,b), (b,a)] ++ symClos xs
-- ??? Am I missing something (time spend: 2 minutes)

symClosTest1 = symClos [(1,2),(2,3),(3,4)] == [(1,2),(2,1),(2,3),(3,2),(3,4),(4,3)]


-- Lab 4 question 4
isSerial :: Eq a => [a] -> Rel a -> Bool
isSerial domain relation = [x | x <- domain, length (getRelations x domain relation) > 0] == domain

getRelations :: Eq a => a -> [a] -> Rel a -> [a]
getRelations x xs a = [y | y <- xs, (x, y) `elem` a]

-- Symmetric Closure generation of domain should always be serial
isSerialTest :: Int -> Bool
isSerialTest y = let x = ((abs y) `mod` 100) + 1 in isSerial [0..x] (symClos [(x, x+1) | x <- [0..(x-1)]])

-- Generate iterative relations that, but no symClos should always be False
isSerialTest2 :: Int -> Bool
isSerialTest2 y = let x = ((abs y) `mod` 100) + 1 in not (isSerial [0..x] [(x, x+1) | x <- [0..(x-1)]])

-- Discuss: relation (x, y) = x == (y `mod` n)
-- Relation is serial when (min domain) < n <= (max domain)
-- number `mod` smallerNumber will always result into a number smaller or equal to smallerNumber, which will be 
-- in the provided domain, also assuming the min.
-- For cases where number < smallerNumber the mod operation does not matter, and number is in relation with itself.


-- Lab 4 question 5
infixr 5 @@
(@@) :: Eq a => Rel a -> Rel a -> Rel a
r @@ s = nub [ (x,z) | (x,y) <- r, (w,z) <- s, y == w ] -- Please provide import declaration next time...

trClos :: Ord a => Rel a -> Rel a 
trClos a
    | a /= cur = trClos cur 
    | otherwise = a
    where cur = sort (nub (a ++ (a @@ a)))

trClosTest1 = trClos [(1,2),(2,3),(3,4)] == [(1,2),(1,3),(1,4),(2,3),(2,4),(3,4)]


-- Lab 4 question 6
-- All elements of xs must be in the symmetric closure of xs
symClosTest2 :: [(Int, Int)] -> Bool
symClosTest2 xs = let symmetricClosure = symClos xs in all (==True) [x `elem` symmetricClosure | x <- xs]

-- For all elements of xs there must be a reverse in the symmetric closure of xs
symClosTest3 :: [(Int, Int)] -> Bool
symClosTest3 xs =  let symmetricClosure = symClos xs in all (==True) [(y, x) `elem` symmetricClosure | (x, y) <- xs] 

-- For all elements of the symmetric closure of xs, the element or its reverse should be in xs (not XOR in case of a symmetric closure 
-- already present in xs).
symClosTest4 :: [(Int, Int)] -> Bool
symClosTest4 xs =  let symmetricClosure = symClos xs in all (==True) [((x, y) `elem` xs) || ((y, x) `elem` xs) | (x, y) <- symmetricClosure, (x /= y) && (x,y) `elem` xs]

-- All elements of xs must be in the transitive closure of xs
trClosTest2 :: [(Int, Int)] -> Bool
trClosTest2 xs = let transitiveClosure = trClos xs in all (==True) [x `elem` transitiveClosure | x <- xs]

-- For any element in xs where pattern [(x, y), (y, z)] occurs, there must be an (x, z) in the transitiveClosre
getRelationsForElement relations element = [y | (x, y) <- relations, x == element] -- Gets all _ where [(x, _), ...]
checkTransRelation element xs = all (==True) [(all (==True) [(element, z) `elem` xs | z <- getRelationsForElement xs y]) | y <- getRelationsForElement xs element] -- Checks pattern (x, y) -> (y, z) => (x, z)

trClosTest3 :: [(Int, Int)] -> Bool
trClosTest3 xs = let transitiveClosure = trClos xs in all (==True) [checkTransRelation x transitiveClosure | (x, _) <- xs] 


-- Lab 4 question 7
{- 
 - For any case where there is no transitive closure in the starting domain, but there
 - is a transitive closure in the symmetric closure of the starting domain, this case 
 - is invalid. This even holds for the serial relation where R = {(x, y) | y `elem` domain}, 
 - implying that every element has a counterpart (the domain is exhaustive). 
 -}
a = [(1,2), (3,2), (3,4)]

symTran1 = symClos a
-- symTran1 = [(1,2),(2,1),(3,2),(2,3),(3,4),(4,3)]
symTran2 = trClos symTran1
-- symTran2 = [(1,1),(1,2),(1,3),(1,4),(2,1),(2,2),(2,3),(2,4),(3,1),(3,2),(3,3),(3,4),(4,1),(4,2),(4,3),(4,4)]

tranSym1 = trClos a
-- tranSym1 = [(1,2),(3,2),(3,4)]
tranSym2 = symClos tranSym1
-- tranSym2 = [(1,2),(2,1),(3,2),(2,3),(3,4),(4,3)]

symTranIsTranSym = symTran2 == tranSym2 -- False

{- 
 - This can actually be proven with a simple completely transitive list like [(1,2), (2,3), (3,4)]. 
 - If the symmetric closure is done first, the transitive part will actually lead that for any x 
 - in (x, y), y is the complete domain.
 -}

main :: IO ()
main = do
    putStrLn "===Exercise 1==="
    print (manualTestRandomIntSet) -- Manual implementation
    quickCheck quickTestRandomIntSet
    testRandomIntSet -- QuickCheck
    putStrLn "===Exercise 2==="
    print (manualPropertyIntersection 12345 (randomIntSet 12345))
    print (manualPropertyUnion 12345 (randomIntSet 12345))
    print (manualPropertyDifference 12345 (randomIntSet 12345))
    print (manualTestSetIntersection2 12345) -- Tests 100 Different sets
    print (manualTestSetUnion2 12345) -- Tests 100 Different sets
    print (manualTestSetDifference2 12345) -- Tests 100 Different sets
    quickCheck testIntersection
    quickCheck testUnion
    quickCheck testDifference
    putStrLn "===Exercise 3==="
    print (symClosTest1) 
    putStrLn "===Exercise 4==="
    quickCheck isSerialTest
    quickCheck isSerialTest2
    putStrLn "===Exercise 5==="
    print (trClosTest1)
    putStrLn "===Exercise 6==="
    quickCheck symClosTest2
    quickCheck symClosTest3
    quickCheck symClosTest4
    quickCheck trClosTest2  
    quickCheck trClosTest3 
    putStrLn "===Exercise 7==="
    print (not symTranIsTranSym)
