-- 103 https://projecteuler.net/problem=103
import Data.List -- subsequences

isValidSubsetByLength :: (Ord a, Num a, Foldable t1, Foldable t) => t1 a -> t a -> Bool 
isValidSubsetByLength a b
    | aLen > bLen = (sum a) > (sum b)
    | bLen > aLen = (sum b) > (sum a)
    | otherwise = True
    where aLen = length a
          bLen = length b

isValidBySubsetSum a b = (sum a) /= (sum b)

-- This is where the work happens. Subsequences takes the longest time.
isValidSet a = let subsets = subsequences a in length [(x, y) | x <- subsets, y <- subsets, (not (isValidSubsetByLength x y)) || (not (isValidBySubsetSum x y)), x /= y] == 0


guessOpts [] = []
guessOpts (x:xs) = [[(x-1)..(x+1)]] ++ (guessOpts xs) -- Change at your own peril.

-- Combos and allDifferent are stolen from the internet
combos [] = [[]]
combos ([]:ls) = combos ls
combos ((h:t):ls) = map (h:) (combos ls) ++ combos (t:ls)
allDifferent :: (Eq a) => [a] -> Bool
allDifferent []     = True
allDifferent (x:xs) = x `notElem` xs && allDifferent xs

combinations guess = let opts = combos (guessOpts guess) in [x | x <- opts, (length x) == (length guess)]


guess bestGuess = [(x, sum x) | x <- (combinations bestGuess), allDifferent x, isValidSet x]

-- [([20,31,38,39,40,42,45],255),([20,31,38,40,39,42,45],255),([20,31,39,38,40,42,45],255),([20,32,39,40,41,43,46],261),([21,32,39,40,41,43,46],262)]
-- guess is actually the guess we found by starting formula: [20,31,38,39,40,42,45],255
euler13 = head (guess [20, 31, 38, 39, 40, 42, 45])
