import Lab5Helper
import Data.List -- subsequences
import Control.Monad -- filterM
import Test.QuickCheck

-- Lab 5 question 1
-- Calculates all mod powers recursively, exponent must be of base 2
powTwoMod x 1 modulo = [x `mod` modulo]
powTwoMod x exponent modulo = 
    let previousPowers = powTwoMod x (exponent `div` 2) modulo 
        previousPower  = head previousPowers 
    in [(previousPower * previousPower) `mod` modulo] ++ previousPowers

-- Calculates which powers of two make up the given n. Gives the positions
-- of the `1`s when converting n to binary.
sumPowTwo 0 = []
sumPowTwo 1 = [0]
sumPowTwo n = 
    let biggest = floor (logBase 2 n)
        remainder = n - 2^biggest
    in [biggest] ++ (sumPowTwo remainder)

--exM :: Integer -> Integer -> Integer -> Integer
exM x exponent modulo = 
    let sums = sumPowTwo exponent
        powers = reverse (powTwoMod x (2^(head sums)) modulo)
    in product (map (\x -> powers !! x) sums) `mod` modulo

-- Some speed comparisons
-- (6^12345) `mod` 5     -- (0.01 secs, 79,176 bytes)
-- exM 6 12345 5         -- (0.01 secs, 87,560 bytes)
--
-- (6^123456789) `mod` 5 -- (3.25 secs, 111,831,472 bytes)
-- exM 6 123456789 5     -- (0.01 secs, 124,248 bytes)

--exMTest :: (Int, Int, Int) -> Bool
--exMTest (a, b, c) = ((a^b) `mod` c) == (exM a b c) -- Good test when type issues exM are fixed..

-- Lab 5 question 2
-- This research https://www.ams.org/journals/mcom/1989-53-187/S0025-5718-1989-0969484-8/S0025-5718-1989-0969484-8.pdf
-- shows a way to generate carmichael numbers, which are the bane of Fermat's Primality Check. The method consists of
-- the multiplication of three primes with certain properties. However, to not only generate False postives we can just
-- multiply three primes together. We can be more efficient if we generate subsequences of the first n primes.
getComposite n = product (drop n (take (n+3) primes))
composites = map getComposite [0..] -- Generates infinite numbers

-- Doesn't generate infinite numbers, but more likely to generate Carmichael numbers
composites' n = sort [product x | x <- (subsequences (take n primes)), length x > 1, length x < 5] 

testFermats :: [Integer] -> IO ()
testFermats [] = print "All tests passed"
testFermats (x:xs) = do
    garbage <- primeTestF x
    if garbage then print (garbage, x) else testFermats xs

-- Lab 5 question 3
foolFermats [] = print "Unable to fool"
foolFermats (x:xs) = do
    --tests = fmap (\k -> primeTestsF k x) [1..3] 
    t1 <- primeTestsF 1 x
    t2 <- primeTestsF 2 x
    t3 <- primeTestsF 3 x
    if t1 && t2 && t3 then print (x) else foolFermats xs
-- foolFermats (composites' 20) -- 91 (lowest found thusfar)

-- primeTestsF 1 101 -- True
-- primeTestsF 3 101 -- True
{-
 - The `k` parameter is the amount of random tests executed.
 -
 - I am not sure how to efficiently do an fmap of IO Bool, hence the horrible
 - t1,t2,t3 bindings. The results are very haphazard and unpredictable, as such
 - is the case with random testing.  
 -}


-- Lab 5 question 4
carmichael :: [Integer]
carmichael = [ (6*k+1)*(12*k+1)*(18*k+1) |
      k <- [2..],
      prime (6*k+1),
      prime (12*k+1),
      prime (18*k+1) ]

testFermatsCarmichael (x:xs) = do
    bind <- primeTestF x
    if bind then print x else testFermatsCarmichael xs
{-
 - The Carmichael numbers satisfy the condition that Fermats uses to check a number
 - for primality. However, the number is in fact not prime. The occurance of these
 - numbers is very rare, more rare in fact than primes. The larger the number gets,
 - the rarer they are. Given that cryptology works with insanely large primes the
 - presense of a Carmichael numbers is tiny. Additionally, if a number does pass the
 - Fermat check one can use another prime check to make sure it is not a Carmichael
 - number.
 -} 

testRmCarmichael [] = print "Unable to fool"
testRmCarmichael (x:xs) = do
    bind <- primeMR 1 x
    if bind then print x else testRmCarmichael xs

{- 
 - Sometimes:
 -     testRmCarmichael (take 2 carmichael) -- 294409
 - But usually the function is just really slow, nevertheless it does not give
 - as many false positives as the Fermats function.
-}


-- Lab 5 question 5
mersenne x = do
    yeah <- primeMR 1 x
    if yeah then return x else return 0 

generateMersennes = do
    let probablePrimes = [2^p-1 | p <- (take 8 primes)]
    yeet <- mapM mersenne probablePrimes
    return (filter (/= 0) yeet)

isqrt x = floor (sqrt (fromIntegral  x))
isPrime :: Integer -> Bool
isPrime k = if k > 1 then null [ x | x <- [2..isqrt k], k `mod` x == 0] else False -- Stolen from StackOverflow
generateMersennesTest = do
    tests <- generateMersennes
    let results = map (\x -> isPrime x) tests
    return (all (==True) results)

-- The generated Mersenne primes are only as strong as the primeMR function.
-- If you are unlucky, it will generate false positives 

data Tree a = T a [Tree a] deriving (Eq,Ord,Show)

grow :: (node -> [node]) -> node -> Tree node
grow step seed = T seed (map (grow step) (step seed))

tree1 n = grow (step1 n) (1,1)
step1 n = \ (x,y) -> if x+y <= n then [(x+y,x),(x,x+y)] else [] -- step function

tree2 n = grow (step2 n) (1,1)
step2 n = \ (x,y) -> if x+y <= n then [(x+y,y),(x,x+y)] else [] -- step function

explodeTree (T x []) = [x]
explodeTree (T x xs) = [x] ++ (concat [explodeTree y | y <- xs])

generateCoprimeSets 0 = [(1,1)]
generateCoprimeSets n = [(x,y) | x <- [1..n], y <- [1..n], coprime x y]

testCoprimeTree1 x = let n = abs x in (sort (explodeTree (tree1 n))) == (sort (generateCoprimeSets n))
testCoprimeTree2 x = let n = abs x in (sort (explodeTree (tree2 n))) == (sort (generateCoprimeSets n))

main :: IO()
main = do
    putStrLn "===Exercise 2==="
    testFermats (sort (take 20 composites))
    putStrLn "===Exercise 3==="
    foolFermats (composites' 20)
    putStrLn "===Exercise 4==="
    testFermatsCarmichael carmichael -- 4.1, This is safe because first test case will fail
    testRmCarmichael (take 1 carmichael) -- 4.3
    putStrLn "===Exercise 5==="
    gmt1 <- generateMersennesTest
    print (gmt1)
    putStrLn "===Exercise 6==="
    quickCheck testCoprimeTree1
    quickCheck testCoprimeTree2
-- main
-- (True,105)
-- 6601

