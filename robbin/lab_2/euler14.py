cache = {}

def calc(number):
	sequence = []
	last = number
	while True:
		if last == 1:
			cache[number] = len(sequence) + 1
			return
		elif last < number:
			cache[number] = len(sequence) + 1 + cache[last]
			return
		elif last % 2 == 0:
			sequence.append(last)
			last = last // 2
		else:
			sequence.append(last)
			last = 3*last+1

largest = (0,0)
for i in range(1, 1000000):
	calc(i)
	if cache[i] > largest[0]:
		largest = (cache[i], i)

print(largest)
