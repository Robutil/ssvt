module Helper where


-- Sourced from earlier assignments
primes :: [Integer]
primes = 2 : filter prime [3..]

prime :: Integer -> Bool
prime n = n > 1 && all (\ x -> rem n x /= 0) xs
  where xs = takeWhile (\ y -> y^2 <= n) primes


nextPrime :: Integer -> Integer
nextPrime n = if prime n then n else nextPrime (n+1)

