import LTS
import Test.QuickCheck
import System.Random

-- reading ( and understanding ) the damn paper +- 7h

-- 1

-- the implementation allows for empties in the definition - which would make an invalid LTS
-- also, the starting state may not exist
-- createLTS will derive the states and transitions from the state transitions, we can assume this is not the case,
-- in LTS's generated with that
stateLessLTS :: LTS
stateLessLTS = ([],["?but", "tea"],[],1);

transitionLessLTS :: LTS
transitionLessLTS = ([1,2],[],[(1,"?but",2)],1);

invalidStartLTS :: LTS
invalidStartLTS = ([1,2,3],["?but","!liq"],[(1,"?but",2),(2,"!liq",3)],6);


-- the paper does not specify reachablilty of states, therefor we have to check only the following:
-- all the states and transitions present in the labeledtransitions must exist in the labels and states
-- the initial state must exist

-- NOTE THAT THIS FUNCTION IS FOR VALIDATING LTS's 
-- THOSE ARE NOT IOTS, THUS, WE DONT HAVE TO DENOTE INPUT OUTPUT,


-- this should validate both those premises, based on pattern matching and recursion
validateLTS :: LTS -> Bool
validateLTS ([],[],c,d) = False -- no transitions or states = immedeatly fail
validateLTS (a,b,c@(hc@(ca,cb,cc):[]),d) = (d `elem` a) && (ca `elem` a) && (cc `elem` a) && (cb `elem` b) -- there is a match
validateLTS (a,b,c@(hc@(ca,cb,cc):cs),d) = (d `elem` a) && (ca `elem` a) && (cc `elem` a) && (cb `elem` b) && validateLTS (a,b,cs,d)
-- recursively check the rest of the list, alternatively: fail
validateLTS (_,_,_,_) = False

-- 2 - jesus' sweaty monad ass titballs damn
-- action :: Gen [Char]
-- action = (choose ("?","!"):(arbitrary::Gen Char))

-- state :: Gen Integer
-- state = choose(1,6)

-- ts :: Gen (Integer, [Char], Integer)
-- ts = (choose(1,6),(choose ("?","!"):(arbitrary::Gen Char)),choose (1,6))

-- ltsGen :: Gen LTS
-- ltsGen = generatedLTS where
--     generatedLTS = createLTS generatedTSL where -- here we refer to LTS 
--         generatedTSL = ((choose(1,6),(choose ("?","!"):(choose ("a", "z")),(choose (1,6)))):generatedTSL) -- here we refer to a TS List, thus TSL

-- ltsGen :: Gen LTS
-- ltsGen = createLTS (sequence (arbitrary ts))

sGen :: Gen State
sGen = do
    x <- choose (1,6)
    return x

-- lGen :: Gen Label
-- lGen = do
--     a <- oneof ['a','b','c'c]
--     return [((oneof ['!','?']):a)]

lGen :: Gen Label
lGen = elements [ "!a", "!b", "!c", "?a", "?b", "?c" ]

tsGen :: Gen LabeledTransition
tsGen = do
    a <- sGen
    b <- lGen
    c <- sGen
    return (a,b,c)

ltsGen :: Gen LTS
ltsGen = do
    x <- listOf tsGen
    return (createLTS x)

