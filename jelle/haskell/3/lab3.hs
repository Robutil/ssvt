import Lecture3

-- 1, total time: 140 minutes

-- contradiction - 15 minutes
-- it is a contradiction if it is never satisfiable
contradiction :: Form -> Bool
contradiction = not . satisfiable

-- tautology - 20 minutes
-- it is a tautology if it satisfies no matter what the input is ( p or not p )
tautology :: Form -> Bool
tautology f = all ( \v -> evl v f ) ( allVals f )

-- entails
-- entails :: Form -> Form -> Bool
-- entails f g = all (( \v -> evl v f ) == ( \v -> evl v g )) ( allVals f )

-- entails, attempt 2 - 75 minutes
-- we generate a list of all the inputs that satisfy g, then we check if
-- all those inputs also satisfy f, if so, then g entails f ( note that )
-- this is not reflective
entails :: Form -> Form -> Bool
entails f g = all ( \v -> evl v f ) ( [ x | x <- (allVals g), ( evl x g ) ] )

-- equivalence - 30 minutes
-- equivalence essentially is reflective entailment, thus if f entails g,
-- and also g entails f then they are equivalent
equiv :: Form -> Form -> Bool
equiv f g = ( entails f g ) && ( entails g f )

-- 2, total time: about 180 min
-- operators explained for ease
-- - == !
-- * == &&
-- + == ||

-- generate a literal - any number 1 to 3
-- generateLiteral :: Integer -> String
-- generateLiteral x = show ((mod x 3) + 1 )

-- generate a negated literal
-- generateNegatedLiteral :: Integer -> String
-- generateNegatedLiteral x = "-" : ( generateLiteral x )

-- gerenateOrClause :: Integer -> String
-- generateOrClause x = "+(" ++ 

generateForm :: Integer -> [Char]
generateForm x
    | x < 0 = generateForm ( abs x )
    | x == 0 = "1"
    | x < 10 = show ((mod x 3) + 1)
    | (mod x 5) == 4 = "-" ++ generateForm (quot x 13)
    | (mod x 5) == 3 = "+(" ++ generateForm (quot x 11 ) ++ " " ++ generateForm ( (quot x 11)-1 ) ++ ")"
    | (mod x 5) == 2 = "*(" ++ generateForm (quot x 7 ) ++ " " ++ generateForm ( (quot x 7)-1 ) ++ ")"
    | (mod x 5) == 1 = "(" ++ generateForm (quot x 5 ) ++ "==>" ++ generateForm ( (quot x 5)-1 ) ++ ")"
    | (mod x 5) == 0 = "(" ++ generateForm (quot x 3) ++ "<=>" ++ generateForm ( (quot x 3)-1 ) ++ ")"
    | otherwise = generateForm ( mod x 100 )

-- generateForm :: Integer -> String
-- generateForm 
