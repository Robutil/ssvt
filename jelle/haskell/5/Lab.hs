import Lecture
import Test.QuickCheck
import System.Random
import Data.List
-- import Criterion.Measurement

genNumberBetween :: Integer -> Integer -> Gen Integer
genNumberBetween x y = do
    z <- choose(x,y)
    return z

-- 1
--recursively solve the problem
-- wikipedia gives us an iterative solution
-- we can reverse that into a recursive one
-- step 1 -> set result one, and a variable x to a
-- if the last bit of b == 1 ( or b modulo 2 == 1 ), then:
--      multiply the result with x, and modulo it by c
-- square x
-- repeat

exM' :: Integer -> Integer -> Integer -> Integer
exM' _ 0 _ = 1
exM' 1 _ _ = 1
exM' a b c
    | (b `mod` 2) == 1 = (a * (exM' a (b `div` 2) c)^2) `mod` c
    | otherwise        = ((exM' a (b `div` 2) c)^2) `mod` c

-- I dont know how to write a performance test for this, however, it is much faster than the original

--quickcheck comparitor
testExM :: (Integer, Integer, Integer) -> Bool
testExM (0, b, c) = testExM (1, b, c)
testExM (a, 0, c) = testExM (a, 1, c)
testExM (a, b, 0) = testExM (a, b, 1)
testExM (a, b, c) = (exM' (abs a) (abs b) (abs c)) == (exM (abs a) (abs b) (abs c))

-- 2
-- to get compositives we have to check if something is prime
-- to check if something is prime we use Fermat's primality test
-- FPT states that a^(p-1) = 1 ( modulo p ), where p is a prime number, and a an arbitrary number
-- we can rewrite this to 1 = a^(p-1) (modulo p)
-- we happened to have made just the function for checking that: exM'
-- if we use the function a couple of times with a random number in a, and the statement holds for 
-- every number then the number in p must be prime
-- each check does not guarantee a prime if the statement holds, however, it does make it more likely
-- if we execute (exM' 38 220 221) (for our random number 38 and prime number 221) we find that the
-- statement holds.
-- This number 38 however, is a Fermat's liar, because when we pick another number, say 29, we find
-- that the statement does not hold.

-- we just keep checking a lower number untill we hit either 1 or a Fermat's liar, which proves it
-- isn't a prime.
-- checking all numbers between 1 and (p-1) will prove that it is a prime or a composite.
-- this check is very inefficient
doCompositeCheck :: Integer -> Integer -> Bool
doCompositiveCheck 0 _ = True -- this is a catch other statement
doCompositiveCheck 1 _ = False
doCompositiveCheck x y = (not ((exM' x (y-1) y) == 1))||(doCompositiveCheck (x-1) y)

composites  :: [Integer]
compositive = [ x |  x <- [1..], doCompositiveCheck (x-1) x ]
-- compositive :: [Integer]
-- -- compositive = [ x | x <- [1..], -(primeTestF x)
-- primeTestFPure :: Integer -> Bool
-- primeTestFPure x 



-- 4
carmichael :: [Integer]
carmichael = [ (6*k+1)*(12*k+1)*(18*k+1) |
    k <- [2..],
    prime (6*k+1),
    prime (12*k+1),
    prime (18*k+1) ]

main :: IO ()
main = do
    putStrLn "===Excercise 1==="
    quickCheck testExM
    -- time_ quickCheck testExM