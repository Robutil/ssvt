module Workshop1 where

import Lecture1
import Test.QuickCheck

sumNSquaredLeft :: Integer -> Integer
sumNSquaredLeft 0 = 0
sumNSquaredLeft n = if n < 0 then 0 else n * n + ( sumNSquaredLeft ( n-1 ) )

sumNSquaredRight :: Integer -> Integer
sumNSquaredRight n = if n < 0 then 0 else ( (n * (n + 1) * (2 * n + 1)) `div` 6 )

sumNSquaredProof :: Integer -> Bool
sumNSquaredProof x = (( sumNSquaredLeft x ) == ( sumNSquaredRight x ))

testReversal :: Integer -> Bool
testReversal n = ((reversal (reversal n)) == (n))