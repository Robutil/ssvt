# Induction Haskell Lab 1

## Proof 2, 3 & 6 workshop

### 2

Prove that for _n_ in natural numbers: 

![formula1](formulas/1.bmp)

Base case ( _n_ = 0 ), holds:

![formula2](formulas/2.bmp)

Now the inductive step, prove that it holds for (_n_ + 1):

![formula3](formulas/3.bmp)

We fill (_n_ + 1) into the left hand, to see where we need to reduce the right hand to. 

![formula4](formulas/4.bmp)

We can separate the common factor of (_n_ + 1):

![formula5](formulas/5.bmp)

Multiply everything by 6 and divide by 6 ( so multiply with 1 ), and reduce:

![formula6](formulas/6.bmp)

![formula7](formulas/7.bmp)

![formula8](formulas/8.bmp)

We can now deduce the upper part of the division:

![formula9](formulas/9.bmp)

And finally:

![formula10](formulas/10.bmp)

Now format it a little differently:

![formula11](formulas/11.bmp)

We found the formula that we were supposed to reduce to, so, we're done, and we've proven that the theorem holds.

### 3

Prove that for _n_ in natural numbers:

![formula12](formulas/12.bmp)

We check the base case, _n_ = 0: 

![formula13](formulas/13.bmp)

That is correct.
We will try to solve for (_n_ + 1), if our assumption is true, then this must be true:

![formula14](formulas/14.bmp)

We now try to reduce the right most part to the middle part. First, we'll work out what is squared between the brackets: 

![formula15](formulas/15.bmp)

Which we can reduce to:

![formula16](formulas/16.bmp)

Now we put the common factor to the front of the equation: 

![formula17](formulas/17.bmp)

Reducing further, and then multiplying with 1 ( 4/4 ): 

![formula18](formulas/18.bmp)

And reducing even further:

![formula19](formulas/19.bmp)

Which can be rewritten to:

![formula20](formulas/20.bmp)

And finally to:

![formula21](formulas/21.bmp)

Which is what we expected it to be,  thus, our assumption is correct.

### 6

We must prove that our formula is divisible by 7, we can express that by saying our function equates to 7 times _something_. It does not matter what _something_ is, we don't have to figure that out, since 7 times _something_ is always  divisible by 7: 

![formula22](formulas/22.bmp)

First, we look at the base case, _n_ = 0: 

![formula23](formulas/23.bmp)

28 is divisible by 7, so this is true.
Now we'll try prove that it holds true for (_n_ + 1), thus, we fill in (_n_ + 1).

![formula24](formulas/24.bmp)

We reduce it a few times:

![formula25](formulas/25.bmp)

We take the original formula out of the result:

![formula26](formulas/26.bmp)

We split the 9 up into 7 and 2 ( for convenience ): 

![formula27](formulas/27.bmp)

We can now see we have 2 times our original formula, and 7 times something else.
Since we assume our original formula is divisible by 7, we rewrite that as 7 times _something_:

![formula28](formulas/28.bmp)

We can now separate the 7 from the rest of the formula like this: 

![formula29](formulas/29.bmp)

Since our whole formula is divisible by 7, our assumption holds true, and thus we've successfully proven that our original formula is divisible by 7.

### 12

For simplicity, we will denote _phi_ as x.

In propositional calculus, a formula is valid if it complies to the following set of rules in **BNF**: 

```
<atom> ::= p | q | r | s | t | u | ...
<operator> ::= <atom> |
            not <operator> |
            ( <operator> and <operator> ) |
            ( <operator> or <operator> ) |
            ( <operator> implies <operator> ) |
            ( <operator> implies reflective <operators> )
```

Essentially, a proper formula is a tree, which branches into subtrees, untill eventually all `<operator>`s devolve into `<atom>`s.

Every different `<operator>` is a subformulae of the starting formula, and so is every `<atom>`.
What follows is that the sum of the `<operator>`s and `<atom>`s is the total amount of subformulae.

We can count this in a simple example to confirm: 
```
not p and q
```

_The root of the formula is the **and** symbol, there we start, we count one. We then look at the left `<operator>`, which is **not**, so we add one again. That `<operator>` evolves into an `<atom>`, **p**, so we count one again, and then finally the right hand of the **and** devolves into a **q**, so we add 1 once more. The result is 4._

Let S(_x_) denote all possible subformulae of a given formula _x_.
Then let's define A(_x_) as all `<atom>`s in a given formula _x_, and C(_x_) all `<operator>`s.

It follows That S(_x_) = A(_x_) + C(_x_).

Let's first define A:

```
A :: <operator> -> Integer
A <atom> = 1
A not x = A x
A x and y = A x + A y
A or = A x + A y
A implies = A x + A y
A implies reflective = A x + A y

```

This just searches the tree untill it finds a `<atom>`, then it returns 1.

Now let's define C:

```
C :: <operator> -> Integer
C <atom> = 0
C not x = 1 + C x
C x and y = 1 + C x + C y
C x or y = 1 + C x + C y
C x implies y = 1 + C x + C y
C x implies reflective y = 1 + C x + C y

```

This just searches the tree untill it finds and atom, and then returns 0, but counting all the steps on it's way.

Now prove that S(_x_) = A(_x_) + C(_x_)